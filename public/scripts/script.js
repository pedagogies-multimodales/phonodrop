///////////////////
// Fonction logg //
///////////////////
const logActivate = true;
function logg(texte) {
    if (logActivate) {
        console.log(texte);
    }
}

var version2 = false

function switchv2(){
    version2 = !version2
    if (version2){ // passage à v2
        console.log("Affichage version 2")
        document.getElementById('switchv2').checked = true
        document.getElementById('ecritoire').style.display = "none"
        document.getElementById('panier').style.display = "none"
        document.querySelectorAll('.param-v1').forEach(div => { div.style.display = "none" })
        document.getElementById('eraseAll').style.display = "none"
        document.getElementById('multidraggable').style.display = "block"
        document.getElementById('panierv2').style.display = "block"
    } else { // passage à v1
        console.log("Affichage version 1")
        document.getElementById('switchv2').checked = false
        document.getElementById('ecritoire').style.display = "block"
        document.getElementById('panier').style.display = "block"
        document.querySelectorAll('.param-v1').forEach(div => { div.style.display = "block" })
        document.getElementById('eraseAll').style.display = "block"
        document.getElementById('multidraggable').style.display = "none"
        document.getElementById('panierv2').style.display = "none"
    }
}

/////////////////////////////////////////////////
//////////// RÉCUPÉRATION DE LA PAGE ////////////
/////////////////////////////////////////////////
// SI url = phonodrop/ → chargement page vierge
// SI url = phonodrop/:id → chargement page vierge puis request infos de la base de données avec phonodrop/:id/page

const thisURL = window.location.href;

var wcarte = '80px'; // à modifier dans css
var marginbot = '00px'; // à modifier dans css
var grille = true; // grille par défaut
var nbrow = 25;
var nbcol = 25;
var dateCreation = false; // sera initialisé lors de l'export, ou récupéré si page déjà enregistrée
var page = new Object();

// dictionnaire répertoriant ce que chaque case contient
let repCases = {};

var pageId = thisURL.match(/id-\w+/);
if (pageId) {
    logg("Page ID : "+pageId);
    // switchv2()
} else {
    logg("Page vierge");
    pageId = '0';
};

if (pageId != "0") {
    $.ajax ({
        //requête qui récupère les infos de la page avec l'identifiant thisURL
        url:'/'+pageId+'/page',
        dataType: 'json'
    }).done( 
        function(data) {
            logg(data);
            wcarte = data.wCarte;
            wcarte = parseInt(wcarte.slice(0,3));
            hcarte = wcarte * 70 / 100;
            document.documentElement.style.setProperty('--wcarte', wcarte + 'px');
            document.documentElement.style.setProperty('--hcarte', hcarte + 'px');

            grille = data.grille;
            marginbot = data.marginBot;
            document.documentElement.style.setProperty('--marginbot', marginbot);
            dateCreation = data.dateCreation;
            page = data.page;

            if ('pagev2on' in data) {
                document.getElementById('multidraggable').innerHTML = initToolDiv + data.pagev2
                switchv2()
            }

            loadPage();
        }
    ).fail(loadPage());
} else {
    loadPage()
    // Passer à la v2 par défaut
    switchv2()
}

function loadPage() {

    logg(grille);
    logg(wcarte);
    logg(page);


    /////////////////////////////////////////////////
    ///////////// GÉNÉRATION DES CASES //////////////
    /////////////////////////////////////////////////

    // Reset sécurité
    document.getElementById('panier').innerHTML = '';
    document.getElementById('casesZone').innerHTML = '';

    for (let i=1; i<=nbrow; i++) {
        var newRow = document.createElement('div');
        newRow.id = "r"+i;
        newRow.classList = "ecritoireRow";
        document.getElementById('casesZone').appendChild(newRow);
        for (let j=1; j<=nbcol; j++) {
            var newCase = document.createElement('div');
            newCase.id = "r"+i+"c"+j;
            newCase.classList = "targetZone case";
            newRow.appendChild(newCase);
        }
    }

    if (grille) {
        grille = !grille;
        toggleGrille();
    }

    /////////////////////////////////////////////////
    /////////////// IMPORT DES CARTES ///////////////
    /////////////////////////////////////////////////
    /// Liste des noms de fichiers dans cartes.js ///
    var panier = document.querySelector('.panier');

    for (bouchePath in bouches) {
        // Création carte & récup infos
        var newDiv = document.createElement('div');
        var phon = bouches[bouchePath].replace('bouche_','phon_').replace('.jpg','');
        
        // Formatage de la carte
        newDiv.id = phon + '-' + Math.random().toString(36).substring(2,9);
        newDiv.className = "carte";
        newDiv.draggable = true;
        newDiv.style.backgroundImage = 'url("bouches/'+bouches[bouchePath]+'")';
        panier.appendChild(newDiv);
    }

    var newLine = document.createElement('hr');
    panier.appendChild(newLine);

    for (i in puncts) {
        // Création carte & récup infos
        var newDiv = document.createElement('div');
        var punct = puncts[i].replace('.jpg','');
        
        // Formatage de la carte
        newDiv.id = punct + '-' + Math.random().toString(36).substring(2,9);
        newDiv.className = "carte";
        newDiv.draggable = true;
        newDiv.style.backgroundImage = 'url("punct/'+puncts[i]+'"';
        panier.appendChild(newDiv);
    }

    var newLine = document.createElement('hr');
    panier.appendChild(newLine);

    for (i in tps) {
        // Création carte & récup infos
        var newDiv = document.createElement('div');
        var tp = tps[i].replace('.jpg','');
        
        // Formatage de la carte
        newDiv.id = tp + '-' + Math.random().toString(36).substring(2,9);
        newDiv.className = "carte";
        newDiv.draggable = true;
        newDiv.style.backgroundImage = 'url("trait-point/'+tps[i]+'"';
        panier.appendChild(newDiv);
    }

    //////////////////////////////////////
    /// Paramétrage des eventlisteners ///
    //////////////////////////////////////

    const cartes = document.querySelectorAll('.carte');
    const cases = document.querySelectorAll('.targetZone');

    // contiendra la carte en déplacement
    var item = null;

    // variable pour mémoriser la div dans laquelle est rentré l'élément en cours de dragging
    var entered = null;


    // On met les eventListeners sur toutes les cases de la page + le panier
    for(const caseX of cases) {
        repCases[caseX.id] = null; // on crée une entrée pour la case dans le répertoire, pour l'instant elle ne contient aucune carte
        caseX.addEventListener('dragover', dragOver);
        caseX.addEventListener('dragenter', dragEnter);
        caseX.addEventListener('dragleave', dragLeave);
        caseX.addEventListener('drop', dragDrop);
    }
    document.querySelector('.panier').addEventListener('drop', dragDrop);

    // On met les eventListeners sur les cartes aussi (dragStart, dragEnd)
    for(const carte of cartes) {
        carte.addEventListener('dragstart', dragStart);
        carte.addEventListener('dragend', dragEnd);
    }

    ///////////////////////////////////////////////////////
    ////////// CHARGEMENT DES CARTES ENREGISTRÉES /////////
    ///////////////////////////////////////////////////////
    for (let i in page) {
        repCases[i] = page[i];
        
        // Création de la carte et append dans la case cible
        var newDiv = document.createElement('div');
        newDiv.id = page[i];
        newDiv.className = "carte";
        newDiv.draggable = true;
        var fileName = '';
        if (page[i].match(/phon_/)) {
            fileName = page[i].replace('phon_','bouche_').replace(/-\w+/,'.jpg');
            newDiv.style.backgroundImage = 'url("bouches/'+fileName+'"';
        } else if (page[i].match(/punct_/)){
            fileName = page[i].replace(/-\w+/,'.jpg');
            newDiv.style.backgroundImage = 'url("punct/'+fileName+'"';
        } else {
            fileName = page[i].replace(/-\w+/,'.jpg');
            newDiv.style.backgroundImage = 'url("trait-point/'+fileName+'"';
        }
        
        newDiv.addEventListener('dragstart', dragStart);
        newDiv.addEventListener('dragend', dragEnd);

        document.getElementById(i).appendChild(newDiv);
    }


    //////////////////////////////////////////
    ////////// Fonctions Drag n Drop /////////
    //////////////////////////////////////////
    // https://www.youtube.com/watch?v=C22hQKE_32c
    // multiple elements : https://www.sitepoint.com/accessible-drag-drop/
    // simple draggable http://jsfiddle.net/maniator/zVZFq/ 

    // PISTES FUTURES :
    // Sélection multiple: https://simonwep.github.io/selection/
    // DragDrop Multiple : https://www.sitepoint.com/accessible-drag-drop/ 


    // Drag Functions
    function dragStart(e) {
        logg("dragStart! from id "+e.target.id);
        this.classList.add('hold');
        if (this.parentNode.classList.contains("case")){
            setTimeout(() => (this.classList.add('invisible')), 0);
        }
        item = e.target;
        logg('item:'+item.id);
        e.dataTransfer.setData('text', '');
    }

    function dragEnd() {
        logg("dragEnd!");
        item.classList.remove('hold');
        item.classList.remove('invisible');
        entered = null;
        item = null;
    }


    /// DU POINT DE VUE DES CASE (CIBLES)
    function dragOver(e) {
        if(item) {
            e.preventDefault();
        }
    }

    function dragEnter(e) {
        // logg("ENTER! in "+this.id);
        e.preventDefault();
        entered = this;
        if (entered) {
            this.classList.add('caseHovered');
        }
    }

    function dragLeave() {
        // logg("LEAVE! from "+this.id);
        this.classList.remove('caseHovered');
        entered = null;
        if (repCases[this.id] == null || repCases[this.id] == item.id) {
            // Si la case est vide, ou si elle contient déjà l'élément, on vide.
            repCases[this.id] = null;
            this.classList.remove('caseFull');
        }
    }

    function dragDrop(e) {
        this.classList.remove('caseHovered');

        if (item.parentNode.classList.contains("case")){
            // Si la carte vient d'une autre case (déplacement)
            if (this.classList.contains('case')){
                // si elle arrive dans une nouvelle case, on remplace
                repCases[item.parentNode.id] = null;
                repCases[this.id] = item.id;
                logg("DROP! in "+this.id+' from '+item.parentNode.id);
                item.classList.remove("invisible");
                e.currentTarget.innerHTML = ''; // On vide la case cible avant d'y poser la carte.
                e.currentTarget.appendChild(item);
            } else {
                // si elle arrive ailleurs (panier etc) : suppression de la carte
                repCases[item.parentNode.id] = null;
                logg("Bye bye !");
                item.parentNode.removeChild(item);
            }

        } else if (this.classList.contains('case')) {
            repCases[item.parentNode.id] = null;
            logg("DROP! in "+this.id);
            var nodeCopy = item.cloneNode(true);
            nodeCopy.classList.remove('hold');
            nodeCopy.addEventListener('dragstart', dragStart);
            nodeCopy.addEventListener('dragend', dragEnd);
            var thisPhon = nodeCopy.id.match(/(\w+)-\w+/);
            thisPhon = thisPhon[1];
            logg("Match id: "+thisPhon);
            thisPhon = thisPhon + '-' + Math.random().toString(36).substring(2,9);
            nodeCopy.id = thisPhon;
            repCases[this.id] = nodeCopy.id;
            logg("Created new item id: "+nodeCopy.id);
            e.currentTarget.innerHTML = ''; // on vide la case si elle déjà pleine.
            e.currentTarget.appendChild(nodeCopy);
            e.preventDefault();
        }
    }

};
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
//                  FONCTIONS GLOBALES                      //

//////////////////////////////////////////////
////////// RESIZABLE DIV PANIER //////////////
//////////////////////////////////////////////
const BORDER_SIZE = 10;
const panel = document.getElementById("tiroir");

let m_pos;
function resize(e){
const dx = m_pos - e.y;
m_pos = e.y;
panel.style.height = (parseInt(getComputedStyle(panel, '').height) + dx) + "px";
}

document.getElementById("hrDiv").addEventListener("mousedown", function(e){
m_pos = e.y;
document.addEventListener("mousemove", resize, false);
}, false);

document.getElementById("hrDiv").addEventListener("mouseup", function(){
    document.removeEventListener("mousemove", resize, false);
}, false);



//////////////////////////////////////////////
////////// PARAMETRES AFFICHAGE //////////////
//////////////////////////////////////////////
function changeSize(factor) {
    var wcarte = getComputedStyle(document.documentElement).getPropertyValue('--wcarte');
    wcarte = parseInt(wcarte.slice(0,3));
    wcarte = wcarte * factor;
    hcarte = wcarte * 70 / 100;
    document.documentElement.style.setProperty('--wcarte', wcarte + 'px');
    document.documentElement.style.setProperty('--hcarte', hcarte + 'px');
}

function eraseAll() {
    for (i in repCases) {
        if (i != "panier"){
            document.getElementById(i).innerHTML = '';
            repCases[i] = null;
        }
    }
}

function mvTiroir(factor) {
    panel.style.height = (parseInt(getComputedStyle(panel, '').height) + factor) + "px";
}

var boolTiroir = true;
function toggleTirroir() {
    if (boolTiroir) {
        panel.style.display = 'none';
        boolTiroir = false;
        document.getElementById('eye0').style.display = "none"
        document.getElementById('eye1').style.display = "block"
    } else {
        panel.style.display = '';
        boolTiroir = true;
        document.getElementById('eye0').style.display = "block"
        document.getElementById('eye1').style.display = "none"
    }
}

function toggleGrille() {
    var cases = document.getElementsByClassName('case');
    for (let i = 0; i<cases.length; i++) {
        cases[i].classList.toggle("caseGrill");
    }
    grille = !grille;
    logg('Grille passe à '+grille);
}

function changeInterligne(factor){
    var marginbot = getComputedStyle(document.documentElement).getPropertyValue('--marginbot');
    marginbot = parseInt(marginbot.slice(0,3));
    marginbot = marginbot + factor;
    document.documentElement.style.setProperty('--marginbot', marginbot + 'px');
    logg('Marginbot passe à '+marginbot);
}

//////////////////////////////////////
////////// ENREGISTREMENT ////////////
//////////////////////////////////////

async function save2url(){
    // RÉCUPÉRATION DES CASES PLEINES
    var casesFull = {}
    for (i in repCases){
        if (repCases[i] != null) {
            casesFull[i] = repCases[i];
            console.log('Plein: '+i+' : '+repCases[i]);
        }
    };

    // SET VARIABLES À ENVOYER
    // Date
    var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var dateTime = date+' '+time;

    // Id
    if (pageId == '0') {
        pageId = 'id-' + Math.random().toString(36).substring(2,9);
        dateCreation = dateTime;
    }

    // Contenu page V2
    document.getElementById('tooldiv').remove
    var pagev2content = document.getElementById('multidraggable').innerHTML
    document.getElementById('multidraggable').innerHTML = initToolDiv + document.getElementById('multidraggable').innerHTML

    // ON EMBALLE TOUT ÇA
    var colis = {
        pageId: pageId,
        wCarte: getComputedStyle(document.documentElement).getPropertyValue('--wcarte'),
        marginBot: getComputedStyle(document.documentElement).getPropertyValue('--marginbot'),
        grille: grille,
        ipCli: '',
        dateCreation: dateCreation,
        dateModif: dateTime,
        page: casesFull,
        pagev2: pagev2content,
        pagev2on: document.getElementById('switchv2').checked
    }
    
    // Paramètres d'envoi
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(colis)
    };
    
    // ENVOI
    const response = await fetch('/phonodrop/export', options)
    const data = await response.json();
    console.log(data);

    // OUVERTURE POPUP AVEC LE LIEN POUR RECHARGER LA PAGE
    getPopUp(data.identifiant);
}

//////////////////////////////////////
////////// PARAMÉTRAGE POPUP /////////
//////////////////////////////////////
// Get the modal
var modal = document.getElementById("myModal");
// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// Fonction pour ouvrir le PopUp 
function getPopUp(lien) {
    document.getElementById('pLien').value = thisURL.replace(pageId, '') + lien;
    modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}
// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

// COPIE AUTO DU LIEN DANS PRESSE-PAPIER
function myFunction() {
    var copyText = document.getElementById("pLien");
    copyText.select();
    copyText.setSelectionRange(0, 99999);
    document.execCommand("copy");
    
    var tooltip = document.getElementById("myTooltip");
    tooltip.innerHTML = "Copié !";
}
    
function outFunc() {
    var tooltip = document.getElementById("myTooltip");
    tooltip.innerHTML = "Copier";
}

// Indiquer le lien de Page Vierge
if(pageId != "0"){
    document.getElementById('hrefNewPage').href = thisURL.replace(pageId, '');
} else {
    document.getElementById('hrefNewPage').href = thisURL;
}

function openInNewTab() {
    var URL = document.getElementById("pLien").value;
    window.open(URL, '_blank');
}
