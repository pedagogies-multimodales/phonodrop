// https://jqueryui.com/draggable/#snap-to
// http://jsfiddle.net/fhamidi/PjmJe/1/

let thisAppli = "phonodrop"

let stress1ponderator = 2
let stress2ponderator = 1.5

//////////////////////////////////////////////////////
///// IMPORT DES CARTES (autres que les tableaux svg)
for (i in puncts) {
    // Création carte & récup infos
    var newDiv = document.createElement('div')
    var punct = puncts[i].replace('.jpg','')
    
    // Formatage de la carte
    newDiv.id = punct
    newDiv.className = "carte-v2-punct"
    newDiv.addEventListener('click', function(){recupPhon(this.id)})
    newDiv.style.backgroundImage = 'url("punct/'+puncts[i]+'"'
    document.getElementById('ponctuation').appendChild(newDiv)
}

for (i in tps) {
    // Création carte & récup infos
    var newDiv = document.createElement('div')
    var tp = tps[i].replace('.jpg','')
    
    // Formatage de la carte
    newDiv.id = tp
    newDiv.className = "carte-v2-punct"
    newDiv.addEventListener('click', function(){recupPhon(this.id)})
    newDiv.style.backgroundImage = 'url("trait-point/'+tps[i]+'"'
    document.getElementById('trait-point').appendChild(newDiv)
}

// Réglettes
let rodColors = [
    "#f3e3ca",
    "#ea1b39",
    "#a8d147",
    "#952a8a",
    "#fdf001",
    "#00834d",
    "#1d1d27",
    "#9a3724",
    "#046cb3",
    "#f27322"
]
let rodHeight = 25

for (i=1; i<=10; i++) {
    // Création carte & récup infos
    var newRod = document.createElement('div')
    var newRect = document.createElement('div')
    var newCircle = document.createElement('div')
    
    // Formatage de la réglette
    newRod.id = `rod${i}`
    newRod.className = "rod"
    newRod.addEventListener('click', function(){recupPhon(this.id)})
    newRod.style.backgroundColor = rodColors[i-1]
    newRod.style.width = `${i*rodHeight}px`
    newRod.style.margin = "5px"

    // Formatage du rectangle
    newRect.id = `rect${i}`
    newRect.className = "rod"
    newRect.addEventListener('click', function(){recupPhon(this.id)})
    newRect.style.backgroundColor = rodColors[i-1]
    newRect.style.width = `${2*rodHeight}px`
    newRect.style.margin = "5px"

    // Formatage du cercle
    newCircle.id = `circle${i}`
    newCircle.className = "rod"
    newCircle.addEventListener('click', function(){recupPhon(this.id)})
    newCircle.style.backgroundColor = rodColors[i-1]
    newCircle.style.width = `${rodHeight}px`
    newCircle.style.borderRadius = "50%"
    newCircle.style.margin = "5px"


    document.getElementById('reglettes').appendChild(newRod)
    document.getElementById('rectangles').appendChild(newRect)
    document.getElementById('ronds').appendChild(newCircle)
}

///////////////////////////////////////////////////


var group = [] // liste des éléments sélectionnés
var lastSelect = "" // dernier élément modifié (pour la position du curseur)



// Paramétrage multidraggable (nécessite multidraggable.js, jquery et jquery-ui)
$('#multidraggable').multidraggable({
    filter: ".carte-v2",
    containment: "#multidraggable",
    onCreate:(event, ui)=>{},

    onSelecting:(event, ui)=>{
        // if (ui.selecting.id != 'tooldiv') //console.log("Sélection ",ui.selecting.id)
    },
    onSelected:(event, ui)=>{
        if (ui.selected.id != 'tooldiv') group.push(ui.selected)
    },

    onStopSelecting:(event, ui)=>{
        if (group.length > 0) getTools(group)
        //console.log("Sélection groupe : "+ group.map((rect)=>{ return rect.id }).join(', '))
    },
    onStartSelecting:(event, ui)=>{
        group = []
    },

    onUnselecting:(event, ui)=>{},
    onUnselected:(event, ui)=>{
        document.querySelector('#tooldiv').style.display = 'none'
        // réinitialisation stress dans l'éditeur
        setStress(0)
    },

    onStartDrag:(event, ui, elements)=>{},
    onDrag:(event, ui, elements)=>{},
    onStopDrag:(event, ui, elements)=>{},
    snap: ".carte-v2",
    stack: ".carte-v2",
})


// Au double-click sur la table: on pose le curseur
document.getElementById('multidraggable').ondblclick = (e) => {
    //console.log("Coucou")
    //console.log("Position curseur (x,y)", e.layerX, e.layerY)
    document.querySelectorAll('.selectedPhon').forEach( (el) => { el.classList.remove('selectedPhon') })
    var anchor = document.getElementById('anchor')
    anchor.style.left = e.layerX + "px"
    anchor.style.top = e.layerY - 28 + "px" // remonter le curseur de la moitié de sa hauteur
    anchor.style.display = "block"
    lastSelect = anchor
}


function sortGroup(groupX){
    return groupX.sort(function(a, b){ return a.offsetLeft - b.offsetLeft})
}

// Écrire une bouche depuis tableau svg
// familles possibles :
//   - do_bouches
//   - do_formes
//   - do_coupes
//   - kinephones
//   - gattegno
//   - pronsci

var firstPhon = true;

function recupPhon(identifiant){
    if (firstPhon) {
        addStat("phonodrop",lang);
        firstPhon = false;
    }

    //console.log(identifiant)
    var newDiv = document.createElement('div')
    
    newDiv.id = identifiant + '-' + Math.random().toString(36).substring(2,9);
    newDiv.classList = "carte-v2"

    var bouche = identifiant.replace('rect_', 'phon_')
    newDiv.classList.add(bouche)
    newDiv.classList.add('noTextClip')

    let ponderateur = 1 // pondérateur de taille pour stress
    if (stress1) {
        ponderateur = stress1ponderator
        newDiv.classList.add('stress1')
        setStress(0)
    } else if (stress2) {
        ponderateur = stress2ponderator
        newDiv.classList.add('stress2')
        setStress(0)
    }

    // PONCTUATION
    if (identifiant.match(/punct_.*/)) {
        newDiv.style.backgroundImage = "url('punct/"+identifiant+".jpg')"

        // Classe PAUSE pour la synthèse vocale
        if (identifiant.match(/.*(espace|exclamation|interrogation|point|virgule)/)) {
            newDiv.classList.add('pause')
        }

        newDivWidth = 80
        newDivHeight = 55
        newDiv.style.width = newDivWidth + "px"
        newDiv.style.height = newDivHeight + "px"

    // TRAIT-POINT
    } else if (identifiant.match(/tp_.*/)) {
        newDiv.style.backgroundImage = "url('trait-point/"+identifiant+".jpg')"
        newDivWidth = 80
        newDivHeight = 55
        newDiv.style.width = newDivWidth + "px"
        newDiv.style.height = newDivHeight + "px"
    
    // REGLETTES
    } else if (identifiant.match(/rod\d+/) || identifiant.match(/rect\d+/) || identifiant.match(/circle\d+/)) {
        let thisRod = document.getElementById(identifiant)
        newDiv.style.backgroundColor = thisRod.style.backgroundColor
        newDiv.style.width = thisRod.style.width
        newDivHeight = rodHeight
        newDiv.style.height = rodHeight+"px"
        newDiv.style.borderRadius = thisRod.style.borderRadius

    // SINOSYLLABE
    } else if (identifiant.match(/syll_.*/)) {
        //console.log("Création de :", identifiant)
        let newSyll = makeNewSyll(currentSyll)
        newDiv.appendChild(newSyll)
        rmPart(document.getElementById('syll1'),'initiale')
        rmPart(document.getElementById('syll2'),'glide')
        rmPart(document.getElementById('syll3'),'tonale')
        rmPart(document.getElementById('syll4'),'finale')
        toggleTone(5)
        newDivWidth = ponderateur * 100
        newDivHeight = ponderateur * 100
        newDiv.style.width = newDivWidth + "px"
        newDiv.style.height = newDivHeight + "px"

        newDiv.classList.add('borderTransparent') // on a déjà la bordure de syll
        erasePinyin() // init PinyinLab, ZhuyinLab

    // PINYIN
    } else if (identifiant.match(/pinyin.*/)) {
        //console.log("Création de :", identifiant)
        newDiv.innerHTML = document.getElementById('pinyinLab').innerHTML;
        newDiv.classList.add("contours");
        newDiv.style.fontSize = "5em";
        newDiv.style.lineHeight = "1.2em";
        // newDiv.style.padding = "0px 5px";
        newDivWidth = ponderateur * 100
        newDivHeight = ponderateur * 110
        // newDiv.style.width = newDivWidth + "px"
        newDiv.style.height = newDivHeight + "px"
        newDiv.borderRadius = "30px"
    
    // ZHUYIN
    } else if (identifiant.match(/zhuyin.*/)) {
        //console.log("Création de :", identifiant)
        newDiv.innerHTML = document.getElementById('zhuyinLab').innerHTML;
        newDiv.classList.add("contours");
        newDiv.style.fontSize = "5em";
        newDiv.style.lineHeight = "1.2em";
        // newDiv.style.padding = "0px 5px";
        newDivWidth = ponderateur * 100
        newDivHeight = ponderateur * 110
        // newDiv.style.width = newDivWidth + "px"
        newDiv.style.height = newDivHeight + "px"
        newDiv.borderRadius = "30px"

    // PHON
    } else if (famille == "kinephones") {
        // rect = w:84 h:26 ry:2.7
        newDivWidth = ponderateur * 84
        newDivHeight = ponderateur * 26
        newDiv.style.width = newDivWidth + "px"
        newDiv.style.height = newDivHeight + "px"
        newDiv.borderRadius = "2.7px"

    } else if (famille == "gattegno") {
        // rect = w:117 h:52 ry:2.5
        newDivWidth = ponderateur * 70
        newDivHeight = ponderateur * 31
        newDiv.style.width = newDivWidth + "px"
        newDiv.style.height = newDivHeight + "px"
        newDiv.borderRadius = "2.5px"

    } else if (famille == "pronsci") {
        // rect = w:74 h:33 ry:2.7
        if ( ["rect_schwi", "rect_schwa", "rect_schwu", "rect_schwdot"].includes(identifiant) ) {
            newDivWidth = 20
            newDivHeight = 20
            newDiv.style.width = newDivWidth + "px"
            newDiv.style.height = newDivHeight + "px"
            newDiv.style.borderRadius = "50%"
        } else {
            newDivWidth = ponderateur * 70
            newDivHeight = ponderateur * 31
            newDiv.style.width = newDivWidth + "px"
            newDiv.style.height = newDivHeight + "px"
            newDiv.style.borderRadius = "2.5px"
        }

        if ( ["rect_dotted1", "rect_dotted2", "rect_schwdot"].includes(identifiant) ) {
            newDiv.style.borderStyle = "dashed"
        }

    } else if (famille == "do_bouches" || famille == "do_formes" || famille == "do_coupes") {
        if ( ["rect_schwi", "rect_schwa", "rect_schwu", "rect_schwdot"].includes(identifiant) ) {
            newDivWidth = 20
            newDivHeight = 20
            newDiv.style.width = newDivWidth + "px"
            newDiv.style.height = newDivHeight + "px"
            newDiv.style.borderRadius = "50%"
        } else {
            var newDivCalq = document.createElement('div')
            newDivCalq.style = "position: absolute; width:100%; height:100%;"
            newDivCalq.style.backgroundImage = "url('cartes/"+famille+"/"+bouche+".png')"
            newDivCalq.style.backgroundRepeat = "no-repeat"
            newDivCalq.style.backgroundPosition = "center"
            newDivCalq.style.backgroundSize = "cover"
            newDivCalq.style.border = "0px transparent solid"
            newDivCalq.style.borderRadius = "5px"
            newDiv.appendChild(newDivCalq)
            
            newDivWidth = ponderateur * 80
            newDivHeight = ponderateur * 55
            newDiv.style.width = newDivWidth + "px"
            newDiv.style.height = newDivHeight + "px"
        }
    }
    
    newDiv.setAttribute('onclick', 'selectPhon(this)')

    if (lastSelect != "" && lastSelect.offsetTop != 0) {
        newDiv.style.left = lastSelect.offsetLeft + lastSelect.offsetWidth + "px"
        newDiv.style.top = lastSelect.offsetTop + lastSelect.offsetHeight/2 - newDivHeight/2 + "px"
    } else {
        newDiv.style.left = "200px"
        newDiv.style.top = "100px"
    }

    // Z-index
    let highestZIndex = getHighestZIndex()
    newDiv.style.zIndex = highestZIndex+1
    document.getElementById('multidraggable').appendChild(newDiv)
    newDiv.click()
}

function selectPhon(rect) {
    document.getElementById('anchor').style.display = "none"
    document.querySelectorAll('.selectedPhon').forEach( (el) => {el.classList.remove('selectedPhon')} )
    rect.classList.add('selectedPhon')
    //console.log("Focus :",rect.id)
    lastSelect = rect
}

function duplicate(groupX){
    group = groupX || group
    var sortedGroup = sortGroup(group)
    var mintop = 10000
    var maxtop = -10000
    sortedGroup.forEach(c => {
        mintop = ((c.offsetTop < mintop) ? c.offsetTop : mintop)
        maxtop = ((c.offsetTop > maxtop) ? c.offsetTop : maxtop)
    })
    var groupHeight = maxtop - mintop
    
    sortedGroup.forEach(card => {
        var newCard = card.cloneNode()
        newCard.id = card.id + '-' + Math.random().toString(36).substring(2,9);
        newCard.classList.remove('ui-selected')
        newCard.innerHTML = card.innerHTML
        newCard.style.left = card.offsetLeft +'px'
        newCard.style.top = card.offsetTop + 80 + groupHeight +'px'
        // newCard.style.transform = "translateY("+groupHeight+"px)"
        newCard.onclick = function(){ selectPhon(this) }
        newCard.ondblclick = function(){ this.remove() }
        document.getElementById('multidraggable').appendChild(newCard)
    })
}

function getGroupRect(groupX){
    group = groupX || group
    var mintop = 10000
    var maxbottom = -10000
    var minleft = 10000
    var maxright = -10000
    group.forEach(c => {
        mintop = ((c.offsetTop < mintop) ? c.offsetTop : mintop)
        maxbottom = ((c.offsetTop+c.offsetHeight > maxbottom) ? c.offsetTop+c.offsetHeight : maxbottom)
        minleft = ((c.offsetLeft < minleft) ? c.offsetLeft : minleft)
        maxright = ((c.offsetLeft+c.offsetWidth > maxright) ? c.offsetLeft+c.offsetWidth : maxright)
    })
    return {top:mintop, bottom:maxbottom, left:minleft, right:maxright, width:maxright-minleft, height:maxbottom-mintop}
}

function getTools(group){
    var groupRect = getGroupRect(group)
    
    var tooldiv = document.getElementById('tooldiv')
    tooldiv.style.top = groupRect.top + 'px'
    tooldiv.style.height = groupRect.height + 'px'
    tooldiv.style.left = groupRect.left + 'px'
    tooldiv.style.width = groupRect.width + 'px'
    tooldiv.style.zIndex = '10000'

    document.querySelectorAll('.noMove').forEach(toto => { toto.style = "" })
    tooldiv.style.display = 'block'

    // Lors de la sélection : afficher le stress dans l'éditeur
    str1 = false
    str2 = false
    for (el in group) {
        if (group[el].classList.contains('stress1')) str1 = true 
        if (group[el].classList.contains('stress2')) str2 = true
    }
    if (str1 && str2) {
        stress1 = false
        document.getElementById('stress1').classList.remove('stressOn')
        stress2 = false
        document.getElementById('stress2').classList.remove('stressOn')
    } else if (str1) {
        stress1 = true
        document.getElementById('stress1').classList.add('stressOn')
        stress2 = false
        document.getElementById('stress2').classList.remove('stressOn')
    } else if (str2) {
        stress1 = false
        document.getElementById('stress1').classList.remove('stressOn')
        stress2 = true
        document.getElementById('stress2').classList.add('stressOn')
    }
}

function removeGroup(groupX){
    group = groupX || group
    group.forEach(card => { card.remove() })
    document.getElementById('tooldiv').style.display = 'none'
}

function alignGroup(groupX){
    group = groupX || group
    var groupRect = getGroupRect(group)
    group.forEach(card => {
        card.style.top = groupRect.top + groupRect.height/2 - card.offsetHeight/2 + 'px'
    })
}

function reverseGroup(groupX){
    group = groupX || group
    var groupRect = getGroupRect(group)
    var ax = groupRect.right
    group.forEach(card => {
        cardleft = card.offsetLeft
        card.style.left = ax +  ax - cardleft - groupRect.width - card.offsetWidth + "px"
    })
}

function initGraph() {
    space()
}

function space() {
    //console.log("Espace")
    document.querySelectorAll('.selectedPhon').forEach( (el) => { el.classList.remove('selectedPhon') })
    var anchor = document.getElementById('anchor')
   
    anchor.style.left = lastSelect.offsetLeft+lastSelect.offsetWidth + document.getElementById('spaceWidth').valueAsNumber + "px"
    anchor.style.top = lastSelect.offsetTop + lastSelect.offsetHeight/2 - 55/2 + "px" // remonter le curseur de la moitié de sa hauteur
    anchor.style.display = "block"
    lastSelect = anchor
}

// PARAMÈTRES DES CARTES (Options à droite de l'écritoire)
function toggleBordures(check){
    // var check = document.getElementById('toggleBordures').checked;
    if (!check.checked) {
        document.documentElement.style.setProperty('--carteV2Border', 'black 0px solid');
    } else {
        document.documentElement.style.setProperty('--carteV2Border', 'black 1px solid');
    }
}

function toggleContours(check){
    // var check = document.getElementById('toggleContours').checked;
    if (!check.checked) {
        document.documentElement.style.setProperty('--contoursColor', 'transparent');
    } else {
        document.documentElement.style.setProperty('--contoursColor', 'black');
    }
}

function togglePinyinLabAPI(check){
    // var check = document.getElementById('togglePinyinLabAPI').checked;
    console.log('togglePinyinLabAPI',check.checked)
    if (!check.checked) {
        document.getElementById('pinyinLabAPI').style.display = "none";
    } else {
        document.getElementById('pinyinLabAPI').style.display = "block";
    }
}

function togglePinyinLab(check){
    console.log('togglePinyinLab',check.checked)
    if (!check.checked) {
        document.getElementById('pinyinLab').style.display = "none";
    } else {
        document.getElementById('pinyinLab').style.display = "block";
    }
}

function setCouleurEcritoire(div, couleur){
    document.documentElement.style.setProperty('--couleurEcritoire', couleur);
    document.querySelectorAll('.colorSettingSelected').forEach( (div) => { div.classList.remove('colorSettingSelected') })
    div.classList.add('colorSettingSelected')
    
    if (couleur == "black" || couleur == "#000000") {
        document.documentElement.style.setProperty('--borders-anchor', "#9b9b9b");
        document.getElementById('toggleContours').checked = false; toggleContours(document.getElementById('toggleContours'));
        document.getElementById('nav-tabContent').style.color = "white";
    } else {
        document.documentElement.style.setProperty('--borders-anchor', "black");
        document.getElementById('nav-tabContent').style.color = "black";
    }
}



function getHighestZIndex(){
  var elems = document.getElementsByClassName('carte-v2');
  var highest = Number.MIN_SAFE_INTEGER || -(Math.pow(2, 53) - 1);
  for (var i = 0; i < elems.length; i++)
  {
    var zindex = Number.parseInt(
      document.defaultView.getComputedStyle(elems[i], null).getPropertyValue("z-index"),
      10
    );
    if (zindex > highest)
    {
      highest = zindex;
    }
  }
  return highest;
}


// GESTION DU STRESS
let stress1 = false
let stress2 = false
function setStress(nb){

    if (nb == 0) {
        // On passe tout à 0 sans se poser de question
        stress1 = false
        document.getElementById('stress1').classList.remove('stressOn')
        stress2 = false
        document.getElementById('stress2').classList.remove('stressOn')

    } else if (nb == 1) {
        if (!stress1) {
            // Si stress1 n'est pas encore actif
            stress1 = true
            document.getElementById('stress1').classList.add('stressOn')
            if (group.length > 0) {
                // si des cartes sont sélectionnées, on les passe à stress1
                for (el in group) { setStressToCarte(group[el],1) }
            }
        } else {
            // Si stress1 est déjà actif : on passe à 0
            stress1 = false
            document.getElementById('stress1').classList.remove('stressOn')
            if (group.length > 0) {
                for (el in group) { setStressToCarte(group[el],0) }
            }
        }
        stress2 = false
        document.getElementById('stress2').classList.remove('stressOn')

    } else if (nb == 2) {
        stress1 = false
        document.getElementById('stress1').classList.remove('stressOn')
        if (!stress2) {
            stress2 = true
            document.getElementById('stress2').classList.add('stressOn')
            if (group.length > 0) {
                for (el in group) { setStressToCarte(group[el],2) }
            }
        } else {
            stress2 = false
            document.getElementById('stress2').classList.remove('stressOn')
            if (group.length > 0) {
                for (el in group) { setStressToCarte(group[el],0) }
            }
        }
    }
    getTools() // on repositionne les outils
}

function setStressToCarte(el,nb){
    if (nb == 0) {
        if (el.classList.contains('stress1')){
            el.classList.remove('stress1')
            el.style.width = parseFloat(el.style.width) / stress1ponderator +'px'
            el.style.height = parseFloat(el.style.height) / stress1ponderator +'px'
            el.style.top = parseFloat(el.style.top) + el.offsetHeight*((stress1ponderator-1)/2) + 'px'
        }
        if (el.classList.contains('stress2')){
            el.classList.remove('stress2')
            el.style.width = parseFloat(el.style.width) / stress2ponderator +'px'
            el.style.height = parseFloat(el.style.height) / stress2ponderator +'px'
            el.style.top = parseFloat(el.style.top) + el.offsetHeight*((stress2ponderator-1)/2) + 'px'
        }
    } else if (nb == 1) {
        if (!el.classList.contains('stress1')){
            setStressToCarte(el,0) // réinitialisation pour partir de 0, et éviter de doubler la taille d'un stress2 en passant à stress1
            el.classList.add('stress1')
            el.style.width = parseFloat(el.style.width) * stress1ponderator +'px'
            el.style.height = parseFloat(el.style.height) * stress1ponderator +'px'
            el.style.top = parseFloat(el.style.top) - el.offsetHeight*((stress1ponderator-1)/(2*stress1ponderator)) + 'px'
        }
    } else if (nb == 2) {
        if (!el.classList.contains('stress2')){
            setStressToCarte(el,0) // réinitialisation pour partir de 0, et éviter de doubler la taille d'un stress2 en passant à stress1
            el.classList.add('stress2')
            el.style.width = parseFloat(el.style.width) * stress2ponderator +'px'
            el.style.height = parseFloat(el.style.height) * stress2ponderator +'px'
            el.style.top = parseFloat(el.style.top) - el.offsetHeight*((stress2ponderator-1)/(2*stress2ponderator)) + 'px'
        }
    }

}