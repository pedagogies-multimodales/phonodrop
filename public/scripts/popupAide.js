/////// POP-UP D'AIDE //////////
var popDivBool = false;
function toggleAides() {
    popDivBool = !popDivBool;
    document.getElementById('aidbtn').classList.toggle("glyphiconSelected");
}

// SVG Do original width="1122.5601"
// SVG Do original height="793.59998"

function showPopDiv(el) {
    if (popDivBool) {
        var svgOriginWidth = 1122.5601;
        var svgOriginHeigth = 793.59998;
        var divWidth = document.getElementById('clavier').offsetWidth;
        var divHeight = document.getElementById('clavier').offsetHeight;
        var elx = el.getAttribute("x");
        var ely = el.getAttribute("y");
        var elWidth = el.getAttribute("width");
        var elHeight = el.getAttribute("height");

        var x = elx * divWidth / svgOriginWidth;
        var y = ely * divWidth / svgOriginWidth;
        var w = elWidth * divWidth / svgOriginWidth;
        var h = elHeight * divHeight / svgOriginHeigth;

        var popDiv = document.getElementById('popDiv');
        popDiv.style.left = x - elWidth / 2 + 'px';
        popDiv.style.top = y + h + 'px';
        popDiv.style.width = divWidth / 4 +'px';
        popDiv.src = "/static/png/aide/" + "aide_" + id2class[el.getAttribute("id")] + ".png";
        popDiv.style.display = "";
    }
}

function hidePopDiv() {
    document.getElementById('popDiv').style.display = "none";
}

//////////////////////////////////////////////////////