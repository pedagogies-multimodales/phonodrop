var api2pinyin = {
    "j":["i","y"],
    "w":["w","u","o"],
    "ɥ":["u","ü","y","yu"],

    "a":["a"],
    "e":["e","i"],
    "ɛ":["e","a","i"],
    "ɤ":["e","u"],
    "i":["i","yi"],
    "ɨ":["i"],
    "o":["o","u","ou","e"],
    "u":["u","wu","o"],
    "y":["u","ü","yu"],
    
    "ŋ":["ng"],
    
    "a˞":["er"],
    "ɤ˞":["er"],
    "ow":["u"],
    "ɯɤ":["e"],
    "wɤ":["o","u"],
    "wo":["o"],
    "wu":["u"],
    "ij":["i"],
    "ji":["i"],
    "ɛj":["i"],
    "ej":["i"],

    "p":["b"],
    "pʰ":["p"],
    "m":["m"],
    "f":["f"],
    "t":["d"],
    "tʰ":["t"],
    "n":["n"],
    "l":["l"],
    "k":["g"],
    "kʰ":["k"],
    "x":["h"],
    "tɕ":["j"],
    "tɕʰ":["q"],
    "ɕ":["x"],
    "tʂ":["zh"],
    "tʂʰ":["ch"],
    "ʂ":["sh"],
    "ɻ":["r"],
    "ʐ":["r"],
    "ts":["z"],
    "tsʰ":["c"],
    "s":["s"]
}