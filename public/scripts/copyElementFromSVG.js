// Copier élément svg du tableau pour le mettre sur la table.

function recupPhon(rect){
    console.log(rect)
    var newDiv = document.createElement('div')
    var newSvg = document.createElementNS("http://www.w3.org/2000/svg", 'svg')
    var newRect = rect.cloneNode()
    
    newDiv.id = newRect.id + '-' + Math.random().toString(36).substring(2,9);
    newDiv.classList = "carte-v2"

    var divDimensions = {
        "w":0,
        "h":0
    }

    divDimensions.w = ( newRect.hasAttribute('width') ? newRect.getAttribute('width') : "80px" )
    divDimensions.h = ( newRect.hasAttribute('height') ? newRect.getAttribute('height') : "50px" )

    // if (rect.lastElementChild.localName == "path") {
    //     console.log("PATH")
    //     newSvg.setAttribute('viewbox', "40 -5 90 120")
    // }

    if (rect.localName == "ellipse") {
        console.log("ELLIPSE")
        newRect.setAttribute("cx", newRect.getAttribute('rx'))
        newRect.setAttribute("cy", newRect.getAttribute('ry'))
        divDimensions.w = newRect.getAttribute('rx')*2
        divDimensions.h = newRect.getAttribute('ry')*2
        newDiv.style.borderRadius = "50%"
    }

    newDiv.style.width = divDimensions.w
    newDiv.style.height = divDimensions.h

    newSvg.setAttribute("width", divDimensions.w)
    newSvg.setAttribute("height", divDimensions.h)
    
    newRect.removeAttribute('id')
    newRect.removeAttribute('onclick')
    newRect.removeAttribute('transform')
    newRect.setAttribute("x","0")
    newRect.setAttribute("y","0")
    newRect.classList = newRect.classList[1]
    newRect.innerHTML = rect.innerHTML

    newSvg.appendChild(newRect)
    newDiv.appendChild(newSvg)
    console.log(newSvg.outerHTML)
    document.getElementById('multidraggable').appendChild(newDiv)
    console.log("ajout ",newDiv)
}