// Create WebSocket connection.
const socket = new WebSocket('ws://127.0.0.1:8080');

// Connection opened
socket.addEventListener('open', function (event) {
    socket.send('Hello Server!');
    console.log('Connected to WS server.')
});

// Listen for messages
socket.addEventListener('message', function (event) {
    console.log('Message from server ', event.data);
});

const sendMessage = () => {
    socket.send('Hello from Client1 !');
}