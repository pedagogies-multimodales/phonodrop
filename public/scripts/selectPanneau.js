var famille = "do_bouches" // indique la représentation phono pour les cartes à créer
var lang = "fr" // pour choisir la langue de synthèse vocale

selectPanneau("phonoFrDo")
document.getElementById('selectPanneau').value = "phonoFrDo"

function selectPanneau(p){
    console.log("selectPanneau",p);

    var svgFrKinephones = document.getElementById('svgFrKinephones'); // Panneau FR Kinephones
    var svgFrGattegno = document.getElementById('svgFrGattegno'); // Panneau FR Gattegno
    var svgEnPronSciBr = document.getElementById('svgEnPronSciBr'); // Panneau EN PronSci
    
    var svgEnAlem = document.getElementById('svgEnAlem'); // Panneau EN ALeM (fond couleurs)
    var pngCalqEnAlemLignes = document.getElementById('pngCalqEnAlemLignes'); // Panneau EN ALeM (lignes)
    var pngPochoirEnAlem = document.getElementById('pngPochoirEnAlem'); // Panneau EN ALeM (formes bouches)
    var svgClickEnAlem = document.getElementById('svgClickEnAlem'); // Panneau EN ALeM (zones clickables)

    var svgClick = document.getElementById('svgClick'); // svg clickable de surface pour Fr A DO
    var pngCalq = document.getElementById('pngCalq'); // png calque 04 pour Fr A Do
    var pngPochoir = document.getElementById('pngPochoir'); // png calque 01 pour Fr A Do
    var svgFond = document.getElementById('svgFond'); // svg fond couleurs pour Fr A Do
    var doCalques = document.getElementById('doCalques'); // boutons calques

    var svgZhLy = document.getElementById('svgZhLy'); // Panneau Mandarin Lyssenko
    var svgZhMa = document.getElementById('svgZhMa'); // Panneau Mandarin Aurélie Mariscalchi
    var svgZhJi = document.getElementById('svgZhJi'); // Panneau Mandarin Shuman Jiao
    var svgZhP7 = document.getElementById('svgZhP7'); // Panneau Mandarin P7 2021

    firstPhon = true; // pour addstat, actif au premier phon cliqué

    if (p == 'phonoFrDo') {
        famille="do_bouches"
        lang="fr"
        // FR DO
        doCalques.style.display = 'block';
        pngCalq.style.display = 'block';
        svgClick.style.display = 'block';
        pngPochoir.style.display = 'block';
        svgFond.style.display = 'block';

        // FR Kinéphones
        svgFrKinephones.style.display = 'none';

        // FR Gattegno
        svgFrGattegno.style.display = 'none';

        // EN ALEM
        svgEnAlem.style.display = 'none';
        pngCalqEnAlemLignes.style.display = 'none';
        pngPochoirEnAlem.style.display = 'none';
        svgClickEnAlem.style.display = 'none';
        // EN PronSci British
        svgEnPronSciBr.style.display = 'none';

        // ZH Lyssenko
        svgZhLy.style.display = 'none';
        // ZH A. Mariscalchi
        svgZhMa.style.display = 'none';
        // ZH S. Jiao
        svgZhJi.style.display = 'none';
        // ZH P7
        svgZhP7.style.display = 'none';
        document.getElementById('nav-zi-tab').style.display = 'none'
		document.getElementById('nav-pinyin-tab').style.display = 'none'
        document.getElementById('nav-zhuyin-tab').style.display = 'none'
        document.getElementById('nav-aide-tab').click()

    } else if (p == 'svgFrKinephones') {
        famille="kinephones"
        lang="fr"
        // FR Kinéphones
        svgFrKinephones.style.display = 'block';

        // FR Gattegno
        svgFrGattegno.style.display = 'none';

        // EN ALEM
        svgEnAlem.style.display = 'none';
        pngCalqEnAlemLignes.style.display = 'none';
        pngPochoirEnAlem.style.display = 'none';
        svgClickEnAlem.style.display = 'none';
        
        // FR DO
        doCalques.style.display = 'none';
        pngCalq.style.display = 'none';
        svgClick.style.display = 'none';
        pngPochoir.style.display = 'none';
        svgFond.style.display = 'none';

        // EN PronSci British
        svgEnPronSciBr.style.display = 'none';

        // ZH Lyssenko
        svgZhLy.style.display = 'none';
        // ZH A. Mariscalchi
        svgZhMa.style.display = 'none';
        // ZH S. Jiao
        svgZhJi.style.display = 'none';
        // ZH P7
        svgZhP7.style.display = 'none';
        document.getElementById('nav-zi-tab').style.display = 'none'
		document.getElementById('nav-pinyin-tab').style.display = 'none'
        document.getElementById('nav-zhuyin-tab').style.display = 'none'
        document.getElementById('nav-aide-tab').click()

    } else if (p == 'svgFrGattegno') {
        famille="gattegno"
        lang="fr"
        // FR Gattegno
        svgFrGattegno.style.display = 'block';
        
        // FR Kinéphones
        svgFrKinephones.style.display = 'none';

        // EN ALEM
        svgEnAlem.style.display = 'none';
        pngCalqEnAlemLignes.style.display = 'none';
        pngPochoirEnAlem.style.display = 'none';
        svgClickEnAlem.style.display = 'none';
        
        // FR DO
        doCalques.style.display = 'none';
        pngCalq.style.display = 'none';
        svgClick.style.display = 'none';
        pngPochoir.style.display = 'none';
        svgFond.style.display = 'none';

        // EN PronSci British
        svgEnPronSciBr.style.display = 'none';

        // ZH Lyssenko
        svgZhLy.style.display = 'none';
        // ZH A. Mariscalchi
        svgZhMa.style.display = 'none';
        // ZH S. Jiao
        svgZhJi.style.display = 'none';
        // ZH P7
        svgZhP7.style.display = 'none';
        document.getElementById('nav-zi-tab').style.display = 'none'
		document.getElementById('nav-pinyin-tab').style.display = 'none'
        document.getElementById('nav-zhuyin-tab').style.display = 'none'
        document.getElementById('nav-aide-tab').click()

    } else if (p == 'phonoEnAlem') {
        famille = "do_formes"
        lang="enbr"
        // EN ALEM
        svgEnAlem.style.display = 'block';
        pngCalqEnAlemLignes.style.display = 'block';
        pngPochoirEnAlem.style.display = 'block';
        svgClickEnAlem.style.display = 'block';

        // FR DO
        doCalques.style.display = 'none';
        pngCalq.style.display = 'none';
        svgClick.style.display = 'none';
        pngPochoir.style.display = 'none';
        svgFond.style.display = 'none';

        // FR Kinéphones
        svgFrKinephones.style.display = 'none';

        // FR Gattegno
        svgFrGattegno.style.display = 'none';

        // EN PronSci British
        svgEnPronSciBr.style.display = 'none';

        // ZH Lyssenko
        svgZhLy.style.display = 'none';
        // ZH A. Mariscalchi
        svgZhMa.style.display = 'none';
        // ZH S. Jiao
        svgZhJi.style.display = 'none';
        // ZH P7
        svgZhP7.style.display = 'none';
        document.getElementById('nav-zi-tab').style.display = 'none'
		document.getElementById('nav-pinyin-tab').style.display = 'none'
        document.getElementById('nav-zhuyin-tab').style.display = 'none'
        document.getElementById('nav-aide-tab').click()
    
    } else if (p == 'phonoEnPronSciBr') {
        famille = "pronsci"
        lang="enbr"

        // EN PronSci British
        svgEnPronSciBr.style.display = 'block';

        // EN ALEM
        svgEnAlem.style.display = 'none';
        pngCalqEnAlemLignes.style.display = 'none';
        pngPochoirEnAlem.style.display = 'none';
        svgClickEnAlem.style.display = 'none';
        
        // FR DO
        doCalques.style.display = 'none';
        pngCalq.style.display = 'none';
        svgClick.style.display = 'none';
        pngPochoir.style.display = 'none';
        svgFond.style.display = 'none';

        // FR Kinéphones
        svgFrKinephones.style.display = 'none';

        // FR Gattegno
        svgFrGattegno.style.display = 'none';

        // ZH Lyssenko
        svgZhLy.style.display = 'none';
        // ZH A. Mariscalchi
        svgZhMa.style.display = 'none';
        // ZH S. Jiao
        svgZhJi.style.display = 'none';
        // ZH P7
        svgZhP7.style.display = 'none';
        document.getElementById('nav-zi-tab').style.display = 'none'
		document.getElementById('nav-pinyin-tab').style.display = 'none'
        document.getElementById('nav-zhuyin-tab').style.display = 'none'
        document.getElementById('nav-aide-tab').click()
    
    } else if (p == 'phonoZhLy') {
        famille = "zhly"
        lang="zh"
        document.getElementById('nav-zi-tab').style.display = 'block'
        document.getElementById('nav-pinyin-tab').style.display = 'none'
        document.getElementById('nav-zhuyin-tab').style.display = 'block'
        document.getElementById('nav-zi-tab').click()

        // ZH Lyssenko
        svgZhLy.style.display = 'block';

        // ZH A. Mariscalchi
        svgZhMa.style.display = 'none';

        // ZH S. Jiao
        svgZhJi.style.display = 'none';

        // ZH P7
        svgZhP7.style.display = 'none';

        // EN PronSci British
        svgEnPronSciBr.style.display = 'none';

        // EN ALEM
        svgEnAlem.style.display = 'none';
        pngCalqEnAlemLignes.style.display = 'none';
        pngPochoirEnAlem.style.display = 'none';
        svgClickEnAlem.style.display = 'none';
        
        // FR DO
        doCalques.style.display = 'none';
        pngCalq.style.display = 'none';
        svgClick.style.display = 'none';
        pngPochoir.style.display = 'none';
        svgFond.style.display = 'none';

        // FR Kinéphones
        svgFrKinephones.style.display = 'none';

        // FR Gattegno
        svgFrGattegno.style.display = 'none';

    } else if (p == 'phonoZhMa') {
        famille = "zhma"
        lang="zh"
        document.getElementById('nav-zi-tab').style.display = 'block'
		document.getElementById('nav-pinyin-tab').style.display = 'none'
        document.getElementById('nav-zhuyin-tab').style.display = 'block'
        document.getElementById('nav-zi-tab').click()
        
        // ZH A. Mariscalchi
        svgZhMa.style.display = 'block';

        // ZH Lyssenko
        svgZhLy.style.display = 'none';

        // ZH S. Jiao
        svgZhJi.style.display = 'none';

        // ZH P7
        svgZhP7.style.display = 'none';

        // EN PronSci British
        svgEnPronSciBr.style.display = 'none';

        // EN ALEM
        svgEnAlem.style.display = 'none';
        pngCalqEnAlemLignes.style.display = 'none';
        pngPochoirEnAlem.style.display = 'none';
        svgClickEnAlem.style.display = 'none';
        
        // FR DO
        doCalques.style.display = 'none';
        pngCalq.style.display = 'none';
        svgClick.style.display = 'none';
        pngPochoir.style.display = 'none';
        svgFond.style.display = 'none';

        // FR Kinéphones
        svgFrKinephones.style.display = 'none';

        // FR Gattegno
        svgFrGattegno.style.display = 'none';
    } else if (p == 'phonoZhJi') {
        famille = "zhji"
        lang="zh"
        document.getElementById('nav-zi-tab').style.display = 'block'
		document.getElementById('nav-pinyin-tab').style.display = 'none'
        document.getElementById('nav-zhuyin-tab').style.display = 'block'
        document.getElementById('nav-zi-tab').click()
        
        // ZH S. Jiao
        svgZhJi.style.display = 'block';
        
        // ZH A. Mariscalchi
        svgZhMa.style.display = 'none';

        // ZH Lyssenko
        svgZhLy.style.display = 'none';

        // ZH P7
        svgZhP7.style.display = 'none';

        // EN PronSci British
        svgEnPronSciBr.style.display = 'none';

        // EN ALEM
        svgEnAlem.style.display = 'none';
        pngCalqEnAlemLignes.style.display = 'none';
        pngPochoirEnAlem.style.display = 'none';
        svgClickEnAlem.style.display = 'none';
        
        // FR DO
        doCalques.style.display = 'none';
        pngCalq.style.display = 'none';
        svgClick.style.display = 'none';
        pngPochoir.style.display = 'none';
        svgFond.style.display = 'none';

        // FR Kinéphones
        svgFrKinephones.style.display = 'none';

        // FR Gattegno
        svgFrGattegno.style.display = 'none';
    } else if (p == 'phonoZhP7') {
        famille = "zhp7"
        lang="zh"
        document.getElementById('nav-zi-tab').style.display = 'block'
		document.getElementById('nav-pinyin-tab').style.display = 'block'
        document.getElementById('nav-zhuyin-tab').style.display = 'block'
        document.getElementById('nav-zi-tab').click()
        
        // ZH S. Jiao
        svgZhJi.style.display = 'none';
        
        // ZH A. Mariscalchi
        svgZhMa.style.display = 'none';

        // ZH Lyssenko
        svgZhLy.style.display = 'none';

        // ZH P7
        svgZhP7.style.display = 'block';

        // EN PronSci British
        svgEnPronSciBr.style.display = 'none';

        // EN ALEM
        svgEnAlem.style.display = 'none';
        pngCalqEnAlemLignes.style.display = 'none';
        pngPochoirEnAlem.style.display = 'none';
        svgClickEnAlem.style.display = 'none';
        
        // FR DO
        doCalques.style.display = 'none';
        pngCalq.style.display = 'none';
        svgClick.style.display = 'none';
        pngPochoir.style.display = 'none';
        svgFond.style.display = 'none';

        // FR Kinéphones
        svgFrKinephones.style.display = 'none';

        // FR Gattegno
        svgFrGattegno.style.display = 'none';
    }
}

////////////////// AFFICHAGE DES CALQUES /////////////////
var rien = document.getElementById('btn-rien');
var vsm = document.getElementById('btn-vsm');
var bch = document.getElementById('btn-bch');
var cpsg = document.getElementById('btn-cpsg');
var pngPochoir = document.getElementById('pngPochoir');
var pngCalq = document.getElementById('pngCalq');

setKeyboards('black');

function setKeyboards(bgColor) {
    if (bgColor == 'black') {
        // Clavier par défaut :
        pngPochoir.src="/png/01.png";
        pngCalq.src="/png/04.png";
        rien.addEventListener('click', function(){pngCalq.src='';btnFocus(rien);famille="do_formes";});
        vsm.addEventListener('click', function(){pngCalq.src="/png/03.png";btnFocus(vsm);famille="do_formes";});
        bch.addEventListener('click', function(){pngCalq.src="/png/04.png";btnFocus(bch);famille="do_bouches";});
        cpsg.addEventListener('click', function(){pngCalq.src="/png/06.png";btnFocus(cpsg);famille="do_coupes";});
    } else if (bgColor == 'white') {
        // Clavier par défaut :
        pngPochoir.src="/png/02.png";
        pngCalq.src="/png/05.png";
        rien.addEventListener('click', function(){pngCalq.src='';btnFocus(rien);});
        vsm.addEventListener('click', function(){pngCalq.src="/png/03.png";btnFocus(vsm);});
        bch.addEventListener('click', function(){pngCalq.src="/png/05.png";btnFocus(bch);});
        cpsg.addEventListener('click', function(){pngCalq.src="/png/06.png";btnFocus(cpsg);});
    };
};

function btnFocus(btn){
    if(rien.classList.contains('btnfocus')){
        rien.classList.toggle('btnfocus');
    };
    if(vsm.classList.contains('btnfocus')){
        vsm.classList.toggle('btnfocus');
    };
    if(bch.classList.contains('btnfocus')){
        bch.classList.toggle('btnfocus');
    };
    if(cpsg.classList.contains('btnfocus')){
        cpsg.classList.toggle('btnfocus');
    };
    btn.classList.toggle('btnfocus');
};
//////////////////////////////////////////////////////////

// Modifier l'encrage et la taille du clavier
function rapetisserPanneau() {
        var clavSize = getComputedStyle(document.documentElement).getPropertyValue('--clavSize');
        var graphSize = getComputedStyle(document.documentElement).getPropertyValue('--graphSize');
        document.documentElement.style.setProperty('--clavSize', parseInt(clavSize.slice(0,2)) - 10 + '%');
        document.documentElement.style.setProperty('--graphSize', parseInt(graphSize.slice(0,2)) + 10 + '%');
}

function agrandirPanneau() {
        var clavSize = getComputedStyle(document.documentElement).getPropertyValue('--clavSize');
        var graphSize = getComputedStyle(document.documentElement).getPropertyValue('--graphSize');
        document.documentElement.style.setProperty('--clavSize', parseInt(clavSize.slice(0,2)) + 10 + '%');
        document.documentElement.style.setProperty('--graphSize', parseInt(graphSize.slice(0,2)) - 10 + '%');
}
