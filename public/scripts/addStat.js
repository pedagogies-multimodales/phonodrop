
async function addStat(modul, lang=lang) {

	// ON EMBALLE TOUT ÇA
    var colis = {
		app: "phonodrop",
        module: modul,
		lang: lang
	}

    // Paramètres d'envoi
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(colis)
    };
    
    // ENVOI
	await fetch('https://phonographe.alem-app.fr/addStat/', options);
}