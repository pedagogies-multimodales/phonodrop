///////////////////
// Fonction logg //
///////////////////
const logActivate = true;
function logg(texte) {
    if (logActivate) {
        console.log(texte);
    }
}

/////////////////////////////////////////////////
///////////// GÉNÉRATION DES CASES //////////////
/////////////////////////////////////////////////
var nombreCases = 100;

for (let i=1; i<=nombreCases; i++) {
    var newCase = document.createElement('div');
    newCase.id = "case"+i;
    newCase.classList = "targetZone case";
    document.getElementById('casesZone').appendChild(newCase);
}


/////////////////////////////////////////////////
/////////////// IMPORT DES CARTES ///////////////
/////////////////////////////////////////////////
var panier = document.querySelector('.panier');

var bouches = [
    "bouche_i.jpg",
    "bouche_e.jpg",
    "bouche_e_maj.jpg",
    "bouche_a.jpg",
    "bouche_y.jpg",
    "bouche_2.jpg",
    "bouche_arobase.jpg",
    "bouche_9.jpg",
    "bouche_u.jpg",
    "bouche_o.jpg",
    "bouche_o_maj.jpg",
    "bouche_a_maj.jpg",
    "bouche_o_maj_nas.jpg",
    "bouche_a_maj_nas.jpg",
    "bouche_e_maj_nas.jpg",
    "bouche_9_nas.jpg",
    "bouche_ij.jpg",
    "bouche_j.jpg",
    "bouche_h_maj.jpg",
    "bouche_w.jpg",
    "bouche_wa.jpg",
    "bouche_we_maj_nas.jpg",
    "bouche_ks.jpg",
    "bouche_gz.jpg",
    "bouche_nj.jpg",
    "bouche_n_maj.jpg",
    "bouche_p.jpg",
    "bouche_b.jpg",
    "bouche_m.jpg",
    "bouche_f.jpg",
    "bouche_v.jpg",
    "bouche_t.jpg",
    "bouche_d.jpg",
    "bouche_n.jpg",
    "bouche_s.jpg",
    "bouche_z.jpg",
    "bouche_l.jpg",
    "bouche_s_maj.jpg",
    "bouche_z_maj.jpg",
    "bouche_k.jpg",
    "bouche_g.jpg",
    "bouche_r.jpg"
];

for (bouchePath in bouches) {
    var phon = bouches[bouchePath].replace('bouche_','phon_').replace('.jpg','');
    var newDiv = makeCarte(phon);
    panier.appendChild(newDiv);
}

function makeCarte(phon) {
    // Création carte & récup infos
    var newDiv = document.createElement('div');
    var bouchePath = phon.replace('phon_','bouche_') + '.jpg';
    
    // Formatage de la carte
    newDiv.id = phon + '-' + Math.random().toString(36).substring(7);
    newDiv.className = "carte";
    newDiv.draggable = true;
    newDiv.style.backgroundImage = 'url("bouches/'+bouchePath+'"';
    return newDiv;
}

//////////////////////////////////////////
/////////////// Drag n Drop //////////////
//////////////////////////////////////////
// https://www.youtube.com/watch?v=C22hQKE_32c
// multiple elements : https://www.sitepoint.com/accessible-drag-drop/

const cartes = document.querySelectorAll('.carte');
const cases = document.querySelectorAll('.targetZone');

//variable for storing the dragging item reference 
//this will avoid the need to define any transfer data 
//which means that the elements don't need to have IDs 
var item = null;

// variable pour mémoriser la div dans laquelle est rentré l'élément en cours de dragging
var entered = null;

// dictionnaire répertoriant ce que chaque case contient
var repCases = {};


// Loop through empties and call drag events
for(const caseX of cases) {
    repCases[caseX.id] = null; // on initialise une entrée pour la case, pour l'instant elle ne contient aucune carte
    caseX.addEventListener('dragover', dragOver);
    caseX.addEventListener('dragenter', dragEnter);
    caseX.addEventListener('dragleave', dragLeave);
    caseX.addEventListener('drop', dragDrop);
}
document.querySelector('.panier').addEventListener('drop', dragDrop);

// Fill listeners
for(const carte of cartes) {
    carte.addEventListener('dragstart', dragStart);
    carte.addEventListener('dragend', dragEnd);
}

// Drag Functions
function dragStart(e) {
    logg("dragStart! from id "+e.target.id);
    // var thisPhon = e.target.id.match(/phon_\w+/);
    // logg("Match id: "+thisPhon);
    // var newCarte = makeCarte(toString(thisPhon));
    // logg("Made new carte! id: "+newCarte.id);

    this.classList.add('hold');
    //setTimeout(() => (this.classList.add('invisible')), 0);
    item = e.target;
    logg('item:'+item.id);
    e.dataTransfer.setData('text', '');
}

function dragEnd() {
    logg("dragEnd!");
    item.classList.remove('hold');
    item.classList.remove('invisible');
    entered = null;
    item = null;
}

/// DU POINT DE VUE DES CASE (CIBLES)

function dragOver(e) {
    if(item) {
        e.preventDefault();
    }
}

function dragEnter(e) {
    // logg("ENTER! in "+this.id);
    e.preventDefault();
    entered = this;
    if (entered) {
        this.classList.add('caseHovered');
    }
}

function dragLeave() {
    // logg("LEAVE! from "+this.id);
    this.classList.remove('caseHovered');
    entered = null;
    if (repCases[this.id] == null || repCases[this.id] == item.id) {
        // Si la case est vide, ou si elle contient déjà l'élément, on vide.
        repCases[this.id] = null;
        this.classList.remove('caseFull');
    }
}

function dragDrop(e) {
    logg("DROP! in "+this.id);
    this.classList.remove('caseHovered');
    
    if (entered != panier) {
        if (repCases[this.id] != null) {
            // Si il y a déjà une carte dans la case, on la remplace (l'ancienne retourne dans le panier)
            logg("Déjà occupé ! On remplace.");
            panier.appendChild(document.getElementById(repCases[this.id]));
        } else {
            this.classList.add('caseFull');
        }
        repCases[this.id] = item.id;
    }
    e.currentTarget.appendChild(item.cloneNode(true));
    e.preventDefault();
}



//////////////////////////////////////////////
////////// RESIZABLE DIV PANIER //////////////
//////////////////////////////////////////////
const BORDER_SIZE = 4;
const panel = document.getElementById("tiroir");

let m_pos;
function resize(e){
  const dx = m_pos - e.y;
  m_pos = e.y;
  panel.style.height = (parseInt(getComputedStyle(panel, '').height) + dx) + "px";
}

panel.addEventListener("mousedown", function(e){
  if (e.offsetY < BORDER_SIZE) {
    m_pos = e.y;
    document.addEventListener("mousemove", resize, false);
  }
}, false);

document.addEventListener("mouseup", function(){
    document.removeEventListener("mousemove", resize, false);
}, false);