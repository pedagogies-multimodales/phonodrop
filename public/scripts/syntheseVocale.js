var currentIpa = "";
var currentLang = "";
var currentDebit = 80;
var voix = "f"

async function playIpa(line="") {
    
    var synth = false

    if (thisAppli == "phonographe") {
        if (thisPageLang == 'fr' || thisPageLang == 'en'){
            // Si quelque chose est écrit...
            var phonEllist = document.getElementsByClassName('text')
            
            if (phonEllist.length > 0) {
                // on récupère tous les élément classe "text" et on récupère leur classe phon_*
                var ipa = "";
                for (e=0; e<phonEllist.length; e++){
                    // on parse les classe c de l'élement e
                    for (c=0; c<phonEllist[e].classList.length; c++){
                        if (phonEllist[e].classList[c].match(/stress1/)){
                            ipa+= 'ˈ'
                        } else if (phonEllist[e].classList[c].match(/stress2/)){
                            ipa+= 'ˌ'
                        }
                    }
                    
                    for (c=0; c<phonEllist[e].classList.length; c++){
                        // si la classe match "phon_.*"" alors on récupère l'équivalent api
                        if (phonEllist[e].classList[c].match(/phon_.*/)){
                            ipa += phon2api[phonEllist[e].classList[c]];
                        } else if (phonEllist[e].classList[c].match(/pause/)) {
                            // si la classe match "pause" on insert une espace (qui sera convertie en <break> dans text2speech.py)
                            ipa += ' ';
                        }
                    }
                }
                ipa = ipa.trim(); // suppression des espaces en début ou en fin
                if (ipa != currentIpa || currentLang != thisPageLang) { synth = true }
            }
        } else window.alert("Cette fonctionnalité n'est pas encore disponible pour cette langue !");
    
    } else if (thisAppli == "phonoplayer"){
        
        var rep = document.getElementById('rep'+line.toString())
        ipa = ""
        for (i=0; i<rep.children.length; i++) {
            var repphon = rep.children[i].classList[1].replace("rect","phon")
            if (repphon != "espace" && repphon != "phon_inconnu") { ipa += phon2api[repphon] }
        }
        if (ipa.length > 0 && ipa != currentIpa) { synth = true }
    
    } else if (thisAppli == "phonodrop") {
        var thisPageLang = lang;
        if (thisPageLang=="enbr") {thisPageLang = "en";}

        // Si quelque chose est écrit...
        if (group.length > 1) {
            group = sortGroup(group)
            // on récupère tous les éléments classe "carte-v2" et on récupère leur classe phon_*
            var ipa = "";
            for (e=0; e<group.length; e++){
                // on parse les classe c de l'élement e
                let newcontent = '';
                for (c=0; c<group[e].classList.length; c++){
                    // si la classe match "phon_.*"" alors on récupère l'équivalent api
                    if (group[e].classList[c].match(/phon_.*/)){
                        newcontent += phon2api[group[e].classList[c]];
                    }
                    if (group[e].classList[c].match(/pause/)) {
                        // si la classe match "pause" on insert une espace (qui sera convertie en <break> dans text2speech.py)
                        newcontent += ' ';
                    }
                    if (group[e].classList[c].match(/stress1/)) newcontent = 'ˈ' + newcontent
                    if (group[e].classList[c].match(/stress2/)) newcontent = 'ˌ' + newcontent
                }
                ipa += newcontent
            }
            ipa = ipa.trim(); // suppression des espaces en début ou en fin
            ipa = ipa.replace(/undefined/g, '')

            if (ipa != currentIpa) { synth = true }

        } else if (group.length == 1) {
            // lire phon kinéphones
            var thisPhon = ""
            for (c=0; c<group[0].classList.length; c++){
                // si la classe match "phon_.*"" alors on récupère l'équivalent api
                if (group[0].classList[c].match(/phon_.*/)){
                    thisPhon = group[0].classList[c]
                }
            }
    
            console.log('Lecture Kinephones', thisPhon, phon2api[thisPhon])
            var audio = new Audio("audio/" + lang+"_" + thisPhon+"-f.mp3")
            audio.play()
        }
    }

    if (synth) {
        currentLang = thisPageLang;
        console.log(`Lecture de [${ipa}]`);
        currentIpa = ipa;
        // ON EMBALLE TOUT ÇA
        var colis = {
            ipa,
            lang: thisPageLang,
            voix: "f",
            'appli': thisAppli
        };
        
        // Paramètres d'envoi
        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(colis)
        };
        
        // ENVOI
        const response = await fetch('https://phonographe.alem-app.fr/_playIpa/', options)
        const data = await response.json();
        readResponse(data["audio"], line);
    
    } else if (group.length > 1) {
        var sv_audio = document.getElementsByTagName("audio")[0];
        sv_audio.playbackRate = document.getElementById('debitParole'+line.toString()).value / 100;
        sv_audio.play();
    }

    if (typeof saveTrace !== "undefined") { 
        saveTrace(`playIpa [${ipa}] ${colis.voix} ${colis.lang} ${document.getElementById('debitParole'+line.toString()).value} rep${line}`)
    }
}

function readResponse(response, line) {
    var source = '<source src="data:audio/mpeg;base64,' + response + '" type="audio/mpeg"></source>';
    var sv_audio = '<audio autoplay="true" controls>' + source + '</audio>';
	sv_audio.playbackRate = document.getElementById('debitParole'+line.toString()).value / 100;
    document.getElementById('audio').innerHTML = sv_audio;
}