let currentSyll = {
    "initiale": { "rect":"", "phon":""},
    "glide": { "rect":"", "phon":""},
    "tonale": { "rect":"", "phon":""},
    "finale": { "rect":"", "phon":""},
    "tone": 0,
    "structure": 0 // millier:initiale, centaine:glide, dizaine:tonale, unité:finale. Ex. 10 = tonale seule; 11 = tonale+finale; 1010 = initiale+tonale 
} // liste des phonèmes 

function makeSyll(code) {
    if (code == 10) [initiale,glide,tonale,finale] = [0,0,1,0]
    if (code == 11) [initiale,glide,tonale,finale] = [0,0,2,2]
    if (code == 111) [initiale,glide,tonale,finale] = [0,3,3,3]
    if (code == 1111) [initiale,glide,tonale,finale] = [4,4,4,4]
    if (code == 110) [initiale,glide,tonale,finale] = [0,2,2,0]
    if (code == 1010) [initiale,glide,tonale,finale] = [2,0,2,0]
    if (code == 1110) [initiale,glide,tonale,finale] = [3,3,3,0]
    if (code == 1011) [initiale,glide,tonale,finale] = [3,0,3,3]

    // transitoires
    if (code == 1000) [initiale,glide,tonale,finale] = [2,0,0,0]
    if (code == 1100) [initiale,glide,tonale,finale] = [3,3,0,0]
    if (code == 1001) [initiale,glide,tonale,finale] = [3,0,0,3]
    if (code == 1101) [initiale,glide,tonale,finale] = [4,4,0,4]
    if (code == 1) [initiale,glide,tonale,finale] = [0,0,0,2]
    if (code == 0) [initiale,glide,tonale,finale] = [0,0,0,0]
    if (code == 100) [initiale,glide,tonale,finale] = [0,2,0,0]
    if (code == 101) [initiale,glide,tonale,finale] = [0,3,0,3]

    console.log(code, initiale,glide,tonale,finale)
    document.getElementById('initiale').classList.remove("_1", "_2", "_3", "_4")
    document.getElementById('glide').classList.remove("_1", "_2", "_3", "_4")
    document.getElementById('tonale').classList.remove("_1", "_2", "_3", "_4")
    document.getElementById('finale').classList.remove("_1", "_2", "_3", "_4")

    document.getElementById('initiale').classList.add("_"+initiale)
    document.getElementById('glide').classList.add("_"+glide)
    document.getElementById('tonale').classList.add("_"+tonale)
    document.getElementById('finale').classList.add("_"+finale)

    document.querySelectorAll('.mybtn.actif').forEach((el) => {el.classList.remove('actif')})
    if (initiale > 0) document.getElementById('syll1').classList.add('actif')
    if (glide > 0) document.getElementById('syll2').classList.add('actif')
    if (tonale > 0) document.getElementById('syll3').classList.add('actif')
    if (finale > 0) document.getElementById('syll4').classList.add('actif')

    currentSyll.structure = code
}

function togglePart(part) {
    if (part == 1000) {
        if (currentSyll.structure < 1000) { makeSyll(1000+currentSyll.structure) } else { makeSyll(currentSyll.structure-1000) }
    }
    if (part == 100) {
        if (currentSyll.structure < 100 || (currentSyll.structure >= 1000 && currentSyll.structure < 1100)) { 
            makeSyll(currentSyll.structure+100) 
        } else {
            makeSyll(currentSyll.structure-100) 
        }
    }
    if (part == 10) {
        if ([0, 1, 100, 101, 1000, 1001, 1100, 1101].includes(currentSyll.structure) ) { makeSyll(currentSyll.structure+10) } else { makeSyll(currentSyll.structure-10) }
    }
    if (part == 1) {
        if (currentSyll.structure < 1 || [10, 110, 100, 110, 1000, 1010, 1100, 1110].includes(currentSyll.structure) ) { makeSyll(currentSyll.structure+1) } else { makeSyll(currentSyll.structure-1) }
    }
    
}

currentTone = 0
function toggleTone(nb) {
    if (nb == currentTone || nb == 5) {
        // console.log("Réinitialisation du ton")
        // réinitialisation si clique sur ton déjà actif
        currentTone = 0
        document.getElementById('ton1').style.display = "none"
        document.getElementById('ton2').style.display = "none"
        document.getElementById('ton3').style.display = "none"
        document.getElementById('ton4').style.display = "none"
        document.getElementById('ton5').style.display = "none"
        document.getElementById('ton6').style.display = "none"
        document.getElementById('ton7').style.display = "none"
        document.getElementById('ton8').style.display = "none"
        currentSyll.tone = 0
    } else {
        currentSyll.tone = nb
        if (nb == 1) {
            currentTone = 1
            document.getElementById('ton1').style.display = "block"
            document.getElementById('ton2').style.display = "none"
            document.getElementById('ton3').style.display = "none"
            document.getElementById('ton4').style.display = "block"
            document.getElementById('ton5').style.display = "none"
            document.getElementById('ton6').style.display = "none"
            document.getElementById('ton7').style.display = "none"
            document.getElementById('ton8').style.display = "none"
        }
        if (nb == 2) {
            currentTone = 2
            document.getElementById('ton1').style.display = "none"
            document.getElementById('ton2').style.display = "block"
            document.getElementById('ton3').style.display = "none"
            document.getElementById('ton4').style.display = "block"
            document.getElementById('ton5').style.display = "none"
            document.getElementById('ton6').style.display = "none"
            document.getElementById('ton7').style.display = "none"
            document.getElementById('ton8').style.display = "none"
        }
        if (nb == 3) {
            currentTone = 3
            document.getElementById('ton1').style.display = "none"
            document.getElementById('ton2').style.display = "block"
            document.getElementById('ton3').style.display = "none"
            document.getElementById('ton4').style.display = "block"
            document.getElementById('ton5').style.display = "none"
            document.getElementById('ton6').style.display = "none"
            document.getElementById('ton7').style.display = "none"
            document.getElementById('ton8').style.display = "block"
        }
        if (nb == 4) {
            currentTone = 4
            document.getElementById('ton1').style.display = "block"
            document.getElementById('ton2').style.display = "none"
            document.getElementById('ton3').style.display = "none"
            document.getElementById('ton4').style.display = "none"
            document.getElementById('ton5').style.display = "none"
            document.getElementById('ton6').style.display = "block"
            document.getElementById('ton7').style.display = "none"
            document.getElementById('ton8').style.display = "none"
        }
    }
    // console.log("Ton:", currentTone)
}

let listinitiales = [
    'rect_p',
    'rect_p_h',
    'rect_m',
    'rect_f',
    'rect_t',
    'rect_t_h',
    'rect_n',
    'rect_l',
    'rect_s',
    'rect_ts',
    'rect_t_hs',
    'rect_s_retr',
    'rect_ts_retr',
    'rect_t_hs_retr',
    'rect_z_retr',
    'rect_s_slash',
    'rect_ts_slash',
    'rect_t_hs_slash',
    'rect_k',
    'rect_x',
    'rect_k_h'
]

let gradientToRight = [
    'phon_ts',
    'phon_t_hs',
    'phon_ts_retr',
    'phon_t_hs_retr',
    'phon_ts_slash',
    'phon_t_hs_slash'
]

let listglides = [
    'rect_j',
    'rect_h_maj',
    'rect_w'
]

let listvoyelles = [
    'rect_i',
    'rect_y',
    'rect_1',
    'rect_u',
    'rect_7',
    'rect_o',
    'rect_a',
    'rect_e',
    'rect_e_maj',
    'rect_wo',
    'rect_m_maj7',
    'rect_7rho',
    'rect_arho',
    'rect_7_rho',
    'rect_a_rho'
]

let listtonales = [
    'rect_i-t',
    'rect_y-t',
    'rect_u-t',
    'rect_e-t',
    'rect_7-t',
    'rect_o-t',
    'rect_e_maj-t',
    'rect_a-t'
]

let listfinales = [
    'rect_rho',
    'rect_j-f',
    'rect_w-f',
    'rect_rho-f',
    'rect_n-f',
    'rect_n_maj-f'
]

let listPinyin = ["a","ai","an","ang","ao","ba","bai","ban","bang","bao","bei","ben","beng","bi","bian","biao","bie","bin","bing","bo","bu","ca","cai","can","cang","cao","ce","cen","ceng","cha","chai","chan","chang","chao","che","chen","cheng","chi","chong","chou","chu","chuai","chuan","chuang","chui","chun","chuo","ci","cong","cu","cuan","cui","cun","cuo","da","dai","dan","dang","dao","de","dei","deng","di","dian","diao","die","ding","diu","dong","dou","du","duan","dui","dun","duo","e","ei","en","eng","er","fa","fan","fang","fei","fen","feng","fo","fou","fu","ga","gai","gan","gang","gao","ge","gei","gen","geng","gong","gou","gu","gua","guai","guan","guang","gui","gun","guo","ha","hai","han","hang","hao","he","hei","hen","heng","hong","hou","hu","hua","huai","huan","huang","hui","hun","huo","ji","jia","jian","jiang","jiao","jie","jin","jing","jiong","jiu","ju","juan","jue","jun","ka","kai","kan","kang","kao","ke","ken","keng","kong","kou","ku","kua","kuai","kuan","kuang","kui","kun","kuo","la","lai","lan","lang","lao","le","lei","leng","li","lia","lian","liang","liao","lie","lin","ling","liu","long","lou","lu","luan","lüe","lun","luo","ma","mai","man","mang","mao","mei","men","meng","mi","mian","miao","mie","min","ming","miu","mo","mou","mu","na","nai","nan","nang","nao","nei","neng","ni","nian","niang","niao","nie","nin","ning","niu","nong","nou","nu","nü","nuan","nüe","nuo","ou","pa","pai","pan","pang","pao","pei","pen","peng","pi","pian","piao","pie","pin","ping","po","pou","pu","qi","qia","qian","qiang","qiao","qie","qin","qing","qiong","qiu","qu","quan","que","qun","ran","rang","rao","re","ren","reng","ri","rong","rou","ru","ruan","rui","run","ruo","sa","sai","san","sang","sao","se","sen","seng","sha","shai","shan","shang","shao","she","shei","shen","sheng","shi","shou","shu","shua","shuai","shuan","shuang","shui","shun","shuo","si","song","sou","su","suan","sui","sun","suo","ta","tai","tan","tang","tao","te","teng","ti","tian","tiao","tie","ting","tong","tou","tu","tuan","tui","tun","tuo","wa","wai","wan","wang","wei","wen","weng","wo","wu","xi","xia","xian","xiang","xiao","xie","xin","xing","xiong","xiu","xu","xuan","xue","xun","ya","yan","yang","yao","ye","yi","yin","ying","yong","you","yu","yuan","yue","yun","za","zai","zan","zang","zao","ze","zei","zen","zeng","zha","zhai","zhan","zhang","zhao","zhe","zhen","zheng","zhi","zhong","zhou","zhu","zhua","zhuai","zhuan","zhuang","zhui","zhun","zhuo","zi","zong","zou","zu","zuan","zui","zun","zuo"]

var possibleCurrentSyll = [""] // pour le pinyin
var possibleOutputs = {} // pour le pinyin : pour chaque pinyin potentiel → <span>p</span><span>i</span>...

function zhPhon(identifiant) {
    let phon = identifiant.replace('rect_', 'phon_').replace('-t', '').replace('-f', '')
    if (gradientToRight.includes(phon)) { phon = phon+'-h' } // gradients à l'horizontal pour affriquées
    // console.log("zhPhon",identifiant, phon)
    
    if (listinitiales.includes(identifiant)) {
        if (document.getElementById('initiale').classList.contains(phon)) {
            document.getElementById('initiale').classList = 'part'
            togglePart(1000)
            currentSyll.initiale.rect = ""
            currentSyll.initiale.phon = ""
        } else {
            if (document.getElementById("initiale").getBoundingClientRect().height > 0) togglePart(1000) // on enlève temporairement si existe déjà
            document.getElementById('initiale').classList = 'part '+phon+' noTextClip'
            togglePart(1000)
            currentSyll.initiale.rect = identifiant
            currentSyll.initiale.phon = phon
        }
    }
    if (listvoyelles.includes(identifiant)) {
        if (document.getElementById('tonale').classList.contains(phon)) {
            document.getElementById('tonale').classList = 'part'
            togglePart(10)
            currentSyll.tonale.rect = ""
            currentSyll.tonale.phon = ""
        } else {
            if (document.getElementById("tonale").getBoundingClientRect().height > 0) togglePart(10) // on enlève temporairement si existe déjà
            document.getElementById('tonale').classList = 'part '+phon+' noTextClip'
            togglePart(10)
            currentSyll.tonale.rect = identifiant
            currentSyll.tonale.phon = phon
        }
    }
    if (listglides.includes(identifiant)) {
        if (document.getElementById('glide').classList.contains(phon)) {
            document.getElementById('glide').classList = 'part'
            togglePart(100)
            currentSyll.glide.rect = ""
            currentSyll.glide.phon = ""
        } else {
            if (document.getElementById("glide").getBoundingClientRect().height > 0) togglePart(100) // on enlève temporairement si existe déjà
            document.getElementById('glide').classList = 'part '+phon+' noTextClip'
            togglePart(100)
            currentSyll.glide.rect = identifiant
            currentSyll.glide.phon = phon
        }
    }
    if (listtonales.includes(identifiant)) {
        if (document.getElementById('tonale').classList.contains(phon)) {
            document.getElementById('tonale').classList = 'part'
            togglePart(10)
            currentSyll.tonale.rect = ""
            currentSyll.tonale.phon = ""
        } else {
            if (document.getElementById("tonale").getBoundingClientRect().height > 0) togglePart(10) // on enlève temporairement si existe déjà
            document.getElementById('tonale').classList = 'part '+phon+' noTextClip'
            togglePart(10)
            currentSyll.tonale.rect = identifiant
            currentSyll.tonale.phon = phon
        }
    }
    if (listfinales.includes(identifiant)) {
        if (document.getElementById('finale').classList.contains(phon)) {
            document.getElementById('finale').classList = 'part'
            togglePart(1)
            currentSyll.finale.rect = ""
            currentSyll.finale.phon = ""
        } else {
            if (document.getElementById("finale").getBoundingClientRect().height > 0) togglePart(1) // on enlève temporairement si existe déjà
            document.getElementById('finale').classList = 'part '+phon+' noTextClip'
            togglePart(1)
            currentSyll.finale.rect = identifiant
            currentSyll.finale.phon = phon
        }
    }

    // ÉCRITURE PINYIN
    var currentSyllonAPI = document.getElementById('pinyinLabAPI').innerText;
    console.log('Current syll:',currentSyllonAPI,possibleCurrentSyll);
    phon = phon.replace('-h', '')
    
    var phonApi = phon2api[phon];
    console.log('PINYIN:',phon,phonApi);
    
    
    // syllabe API attestée ?
    var possibleSyllonApi2pinyin = {} // contiendra tous les syllons Api 2 pinyin possibles étant donnée currentSyllonAPI
    var myRegAPI = new RegExp("^"+currentSyllonAPI+phonApi+".*$");
    for (x in syllonAPI2pinyin) {
        if(x.match(myRegAPI)){
            possibleSyllonApi2pinyin[x] = syllonAPI2pinyin[x];
        }
    }
    console.log("Syllons API possibles:",possibleSyllonApi2pinyin);
    document.getElementById('pinyinLabAPI').innerHTML += phonApi;
    var pinyinLab = document.getElementById('pinyinLabAPI').innerHTML;

    // Construction du pinyin en cours à partir des phonèmes sélectionnés
    // pour chaque phonème API, prendre le premier équivalent pinyin compatible étant donné le contexte gauche et droit
    var pinyinEnCoursHtml = "";
    var pinyinEnCours = "";

    for (xph=0; xph<pinyinLab.length; xph++) {
        // pour chaque api dans pinyinLab
        var ph = pinyinLab[xph];
        if (pinyinLab[xph+1]=="ʰ") {
            ph = pinyinLab[xph]+pinyinLab[xph+1];
            xph++;
        } else if (pinyinLab[xph+2]=="ʰ") {
            ph = pinyinLab[xph]+pinyinLab[xph+1]+pinyinLab[xph+2];
            xph+=2;
        
        // phonèmes à traiter par deux sans condition
        } else if (["ts","tɕ","tʂ","ɯɤ","a˞","ɤ˞"].includes(pinyinLab[xph] + pinyinLab[xph+1])) {
            ph = pinyinLab[xph]+pinyinLab[xph+1];
            xph++;

        // cas du wo (bo po mo fo)
        } else if (pinyinLab[xph] + pinyinLab[xph+1] == "wo" && ["pwo","pʰwo","mwo","fwo"].includes(pinyinLab)) {
            ph = pinyinLab[xph]+pinyinLab[xph+1];
            xph++;

        // cas des types "dui" twɛj (exception: "wei"), "niu" njow (exception: "you")
        } else if (["wɛj","wej","jow"].includes(pinyinLab[xph-1] + pinyinLab[xph] + pinyinLab[xph+1]) && pinyinLab.length>3) {
            ph = pinyinLab[xph]+pinyinLab[xph+1];
            xph++;
        
        // cas du type ling ljiŋ (exception: "yi")
        } else if (["ji"].includes(pinyinLab[xph] + pinyinLab[xph+1]) && xph>0) {
            ph = pinyinLab[xph]+pinyinLab[xph+1];
            xph++;
        
        // cas du type "gong" kwoŋ (exception: "weng" woŋ)
        } else if (["woŋ","wuŋ","wɤŋ","wun","wɤn"].includes(pinyinLab[xph] + pinyinLab[xph+1] + pinyinLab[xph+2]) && pinyinLab.length>3) {
            ph = pinyinLab[xph]+pinyinLab[xph+1];
            xph++;
        }
        
        console.log("pp=",ph);

        for (xpi in api2pinyin[ph]) {
            // pour chaque graphie pi possible pour cet api
            var pi = api2pinyin[ph][xpi];
            
            // on regarde si au moins une valeur de possibleSyllonApi2pinyin commence par ce pinyin
            var booboo = false; // passe à true si une des syllabes pinyin possible commence par contextGauche+pi
            
            var myReg = new RegExp("^"+pinyinEnCours+pi+".*$");
            for ([x,y] of Object.entries(possibleSyllonApi2pinyin)) {
                if(y.match(myReg)) booboo = true;
            }
            
            if (booboo) {
                // Cas du yu/yuan/yue, ne pas prendre la première graphie si yuan/yue
                if (pi == "y" && ["ɥɛn","ɥɛ","ɥan","ɥa"].includes(pinyinLab)) {
                    continue;
                } else {
                    var tonale = "";
                    console.log(ph);
                    if (tonalesApi.includes(ph)) tonale = " tonale";
                    pinyinEnCoursHtml += "<span class='"+ api2class[ph] + tonale +"'>" + pi + "</span>";
                    pinyinEnCours += pi;
                    break;
                }
            }
        }
    }

    document.getElementById('pinyinLab').innerHTML = pinyinEnCoursHtml;


    // ÉCRITURE ZHUYIN
    // Construction du zhuyin en cours à partir des phonèmes sélectionnés
    // 
    var zhuyinEnCours = "";
    var zhuyinEnCoursHtml = "";

    var newPhon = "";

    if (listfinales.includes(identifiant)) {
        console.log("ZHUYIN FINALE")
        newPhon = phonLab[phonLab.length-1] + phon.replace('phon_','')
        console.log("Merging with precedent phon:",newPhon)
        phonLab.pop()
        phon = newPhon;

        document.getElementById('zhuyinLab').removeChild(document.getElementById('zhuyinLab').lastChild);

        // Exceptions
        if (phon=="phon_on_maj") {
            document.getElementById('zhuyinLab').innerHTML += `<span class='phon_o'>ㄨ</span><span class='phon_n_maj'>ㄥ</span>`;
        } else if (phon=="phon_in") {
            document.getElementById('zhuyinLab').innerHTML += `<span class='phon_i'>ㄧ</span><span class='phon_n'>ㄣ</span>`;
        } else if (phon=="phon_in_maj") {
            document.getElementById('zhuyinLab').innerHTML += `<span class='phon_i'>ㄧ</span><span class='phon_n_maj'>ㄥ</span>`;
        } else if (phon=="phon_jyn") {
            document.getElementById('zhuyinLab').innerHTML += `<span class='phon_jy'>ㄩ</span><span class='phon_n'>ㄣ</span>`;
        } else if (phon=="phon_jin") {
            document.getElementById('zhuyinLab').innerHTML += `<span class='phon_ji'>ㄧ</span><span class='phon_n'>ㄣ</span>`;
        } else if (phon=="phon_jin_maj") {
            document.getElementById('zhuyinLab').innerHTML += `<span class='phon_ji'>ㄧ</span><span class='phon_n_maj'>ㄥ</span>`;
        } else if (phon=="phon_h_majyn") {
            document.getElementById('zhuyinLab').innerHTML += `<span class='phon_h_maji'>ㄩ</span><span class='phon_n'>ㄣ</span>`;
        } else if (phon=="phon_h_majyn_maj") {
            document.getElementById('zhuyinLab').innerHTML += `<span class='phon_h_maji'>ㄩ</span><span class='phon_n_maj'>ㄥ</span>`;
        } else if (phon=="phon_won_maj") {
            document.getElementById('zhuyinLab').innerHTML += `<span class='phon_wo'>ㄨ</span><span class='phon_n_maj'>ㄥ</span>`;
        } else if (phon=="phon_wun") {
            document.getElementById('zhuyinLab').innerHTML += `<span class='phon_wu'>ㄨ</span><span class='phon_n'>ㄣ</span>`;
        } else if (phon=="phon_wun_maj") {
            document.getElementById('zhuyinLab').innerHTML += `<span class='phon_wu'>ㄨ</span><span class='phon_n_maj'>ㄥ</span>`;
        } else if (phon=="phon_un") {
            document.getElementById('zhuyinLab').innerHTML += `<span class='phon_u'>ㄨ</span><span class='phon_n'>ㄣ</span>`;
        } else if (phon=="phon_un_maj") {
            document.getElementById('zhuyinLab').innerHTML += `<span class='phon_u'>ㄨ</span><span class='phon_n_maj'>ㄥ</span>`;
        
        } else {
            document.getElementById('zhuyinLab').innerHTML += `<span class='${phon}'>${phon2zhuyin[phon]}</span>`;
        }

    } else if (listtonales.includes(identifiant) || listvoyelles.includes(identifiant)) {
        console.log("ZHUYIN TONALE")
        if (phon=="phon_1") {
            phon = phonLab[phonLab.length-1] + "1";
            document.getElementById('zhuyinLab').removeChild(document.getElementById('zhuyinLab').lastChild);
            document.getElementById('zhuyinLab').innerHTML += `<span class='${phon}'>${phon2zhuyin[phonLab[phonLab.length-1]]}</span>`;
            phonLab.pop();
        } else if (phonLab[phonLab.length-1]=="phon_j" && phon=="phon_i") {
            document.getElementById('zhuyinLab').removeChild(document.getElementById('zhuyinLab').lastChild);
            document.getElementById('zhuyinLab').innerHTML += `<span class='phon_ji'>ㄧ</span>`;
            phon = "phon_ji";
            phonLab.pop();
        } else if (phonLab[phonLab.length-1]=="phon_j" && phon=="phon_y") {
            document.getElementById('zhuyinLab').removeChild(document.getElementById('zhuyinLab').lastChild);
            document.getElementById('zhuyinLab').innerHTML += `<span class='phon_jy'>ㄩ</span>`;
            phon = "phon_jy";
            phonLab.pop();
        } else if (phonLab[phonLab.length-1]=="phon_h_maj" && phon=="phon_y") {
            document.getElementById('zhuyinLab').removeChild(document.getElementById('zhuyinLab').lastChild);
            document.getElementById('zhuyinLab').innerHTML += `<span class='phon_h_majy'>ㄩ</span>`;
            phon = "phon_h_majy";
            phonLab.pop();
        // } else if (phonLab[phonLab.length-1]=="phon_w" && phon=="phon_o") {
        //     document.getElementById('zhuyinLab').removeChild(document.getElementById('zhuyinLab').lastChild);
        //     document.getElementById('zhuyinLab').innerHTML += `<span class='phon_wo'>ㄨ</span>`;
        //     phon = "phon_wo";
        //     phonLab.pop();
        } else if (phonLab[phonLab.length-1]=="phon_w" && phon=="phon_u") {
            document.getElementById('zhuyinLab').removeChild(document.getElementById('zhuyinLab').lastChild);
            document.getElementById('zhuyinLab').innerHTML += `<span class='phon_wu'>ㄨ</span>`;
            phon = "phon_wu";
            phonLab.pop();
        } else {
            document.getElementById('zhuyinLab').innerHTML += `<span class='${phon}'>${phon2zhuyin[phon]}</span>`;
        }
        
    } else {
        console.log("ZHUYIN INITIALE/GLIDE")
        document.getElementById('zhuyinLab').innerHTML += `<span class='${phon}'>${phon2zhuyin[phon]}</span>`;
    }
    
    phonLab.push(phon)

    if (document.getElementById('pinyinLab').innerHTML.length == 0) erasePinyin();

    if (Object.keys(syllons).includes(document.getElementById('pinyinLab').innerText+"5")) {
        document.getElementById('pinyinLab').classList.add("pinyinAtteste");
    } else {
        document.getElementById('pinyinLab').classList.remove("pinyinAtteste");
    }

}

var phonLab = [] // mémoire des phons du syllon en cours

var phon2zhuyin = {
    "phon_p" : "ㄅ",
    "phon_p_h" : "ㄆ",
    "phon_m" : "ㄇ",
    "phon_f" : "ㄈ",
    "phon_v" : "万",
    "phon_t" : "ㄉ",
    "phon_t_h" : "ㄊ",
    "phon_n" : "ㄋ",
    "phon_l" : "ㄌ",
    "phon_k" : "ㄍ",
    "phon_k_h" : "ㄎ",
    "phon_n_maj" : "兀",
    "phon_x" : "ㄏ",
    "phon_ts_slash" : "ㄐ",
    "phon_t_hs_slash" : "ㄑ",
    "phon_j_maj" : "广",
    "phon_s_slash" : "ㄒ",
    "phon_ts_retr" : "ㄓ",
    "phon_t_hs_retr" : "ㄔ",
    "phon_s_retr" : "ㄕ",
    "phon_z_retr" : "ㄖ",
    "phon_ts" : "ㄗ",
    "phon_t_hs" : "ㄘ",
    "phon_s" : "ㄙ",
    "phon_a" : "ㄚ",
    "phon_o_maj" : "ㄛ",
    "phon_wo_maj" : "ㄛ",
    "phon_wo" : "ㄛ",
    "phon_o" : "ㄛ",
    "phon_7" : "ㄜ",
    "phon_m_maj7" : "ㄜ",
    "phon_e_maj" : "ㄝ",
    "phon_e" : "ㄝ",
    "phon_aj" : "ㄞ",
    "phon_ej" : "ㄟ",
    "phon_e_majj" : "ㄟ",
    "phon_aw" : "ㄠ",
    "phon_ow" : "ㄡ",
    "phon_an" : "ㄢ",
    "phon_7n" : "ㄣ",
    "phon_an_maj" : "ㄤ",
    "phon_7n_maj" : "ㄥ",
    "phon_en" : "ㄢ",
    "phon_e_majn" : "ㄢ",
    "phon_a_rho" : "ㄦ",
    "phon_arho" : "ㄦ",
    "phon_7_rho" : "ㄦ",
    "phon_7rho" : "ㄦ",
    "phon_i" : "ㄧ",
    "phon_u" : "ㄨ",
    "phon_y" : "ㄩ",
    "phon_j" : "ㄧ",
    "phon_w" : "ㄨ",
    "phon_h_maj" : "ㄩ"
}
var zhuyinFinales = [
    "phon_w",
    "phon_j",
    "phon_n",
    "phon_n_maj",
    "phon_rho"
]


function validPinyin() {
    recupPhon("pinyin");
    erasePinyin();
}
function validZhuyin() {
    recupPhon("zhuyin");
    erasePinyin();
}

function erasePinyin() {
    $("#syll1").click();
    $("#syll2").click();
    $("#syll3").click();
    $("#syll4").click();
    toggleTone(5);
    document.getElementById('pinyinLab').innerHTML = '';
    document.getElementById('pinyinLabAPI').innerHTML = '';
    document.getElementById('pinyinLab').classList.remove("pinyinAtteste");
    document.getElementById('zhuyinLab').innerHTML = '';
    phonLab = [];

    // init Sinosyllab
    // rmPart(document.getElementById('syll1'),'initiale')
    // rmPart(document.getElementById('syll2'),'glide')
    // rmPart(document.getElementById('syll3'),'tonale')
    // rmPart(document.getElementById('syll4'),'finale')
    // toggleTone(5)
}

//["","́","́","̌","̌",""] // tons[1]="´"  tons[0|5]=""
var tons = [
    {"a":"a","e":"e","er":"er","i":"i","o":"o","u":"u","ü":"ü","yu":"yu","wu":"wu", "yi":"yi"},
    {"a":"ā","e":"ē","er":"ēr","i":"ī","o":"ō","u":"ū","ü":"ǖ","yu":"yū","wu":"wū", "yi":"yī"},
    {"a":"á","e":"é","er":"ér","i":"í","o":"ó","u":"ú","ü":"ǘ","yu":"yú","wu":"wú", "yi":"yí", "m":"ḿ"},
    {"a":"ǎ","e":"ě","er":"ěr","i":"ǐ","o":"ǒ","u":"ǔ","ü":"ǚ","yu":"yǔ","wu":"wǔ", "yi":"yǐ"},
    {"a":"à","e":"è","er":"èr","i":"ì","o":"ò","u":"ù","ü":"ǜ","yu":"yù","wu":"wù", "yi":"yì"},
    {"a":"a","e":"e","er":"er","i":"i","o":"o","u":"u","ü":"ü","yu":"yu","wu":"wu", "yi":"yi"}
]
var tonalesApi = ["a","e","ɛ","ɤ","i","ɨ","o","u","y","a˞","ɤ˞","ow","ɯɤ","wɤ","wo","wu","ij","ji","ɛj","ej","m"];
var tonsZhuyin = ["","ˉ","ˊ","ˇ","ˋ","˙"];

function recupTone(identifiant) {
    var myton = identifiant.slice(-1);
    toggleTone(myton);

    // Écriture en pinyin
    var inPin = document.getElementById('pinyinLab').innerText;
    var inSyll = inPin+myton;
    if (Object.keys(syllons).includes(inSyll)) {
        console.log("Syllon attesté !")
        var nods = document.getElementById('pinyinLab').childNodes
        nods.forEach((nod) => { 
            if ((["a","e","i","o","u","ü","er","yu","wu","yi"].includes(nod.innerText) || (nods.length==1 && nod.innerText=="m")) && nod.classList.contains('tonale')) {
                var thisVoy = nod.innerText;
                console.log("TONALE!",thisVoy)
                nod.innerText = nod.innerText.replace(thisVoy,tons[myton][thisVoy])
            }
        })
        document.getElementById('pinyinLabAPI').innerHTML += myton;
        document.getElementById('pinyinLab').classList.add("pinyinAtteste");

        // ZHUYIN
        var mytonPhon = phonLab[phonLab.length-1]
        if (mytonPhon[mytonPhon.length-1]=="1") { 
            // Cas de si shi tsi chi ri : ton couleur phon_1
            mytonPhon = "phon_1"
        }
        if (myton==5) {
            zhuyinLabhtml = document.getElementById('zhuyinLab').innerHTML
            document.getElementById('zhuyinLab').innerHTML = `<span class='${mytonPhon}'>${tonsZhuyin[myton]}</span>`+zhuyinLabhtml;
        } else if(myton!="1") {
            document.getElementById('zhuyinLab').innerHTML += `<span class='${mytonPhon}'>${tonsZhuyin[myton]}</span>`;
        }
    } else {
        console.log("Syllon non attesté.");
        document.getElementById('pinyinLab').classList.remove("pinyinAtteste");
    }
}

function rmPart(thisEl, syllid) {
    if (thisEl.classList.contains("actif")) {
        // console.log("rm", syllid, currentSyll[syllid].rect)
        zhPhon(currentSyll[syllid].rect)
    }
}

function makeNewSyll(syll) {
    let newSyll = document.createElement('div')
    newSyll.classList = "syll"

    let newInitiale = document.createElement('div')
    let newGlide = document.createElement('div')
    let newTonale = document.createElement('div')
    let newFinale = document.createElement('div')

    structString = syll.structure.toString()

    if (structString == "0") {
        prop = "_1"
    } else if (syll.tonale.phon == "") {
        prop = "_" + (structString.match(/1/g).length+1)
    } else {
        prop = "_" + structString.match(/1/g).length
    }

    if (structString.length >= 4 && structString[structString.length-4] == 1) newInitiale.classList = prop + ' ' + syll.initiale.phon + ' noTextClip'
    if (structString.length >= 3 && structString[structString.length-3] == 1) newGlide.classList = prop + ' ' + syll.glide.phon + ' noTextClip'
    if (structString.length >= 2 && structString[structString.length-2] == 1) newTonale.classList = prop + ' ' + syll.tonale.phon + ' noTextClip'
    if (structString.length >= 1 && structString[structString.length-1] == 1) newFinale.classList = prop + ' finale ' + syll.finale.phon + ' noTextClip'

    newSyll.append(newInitiale, newGlide, newTonale, newFinale)

    let newDivTons = document.createElement('div')
    newDivTons.classList.add('divTons')
    if (syll.tone == 1){ addTone(newDivTons, 't1'); addTone(newDivTons, 't4')}
    if (syll.tone == 2){ addTone(newDivTons, 't2'); addTone(newDivTons, 't4')}
    if (syll.tone == 3){ addTone(newDivTons, 't2'); addTone(newDivTons, 't8'); addTone(newDivTons, 't4')}
    if (syll.tone == 4){ addTone(newDivTons, 't1'); addTone(newDivTons, 't6')}

    function addTone(el, toneEl) {
        let newTone = document.createElement('div')
        newTone.classList.add('ton')
        newTone.style.display = 'block'
        newTone.classList.add(toneEl)
        el.appendChild(newTone)
    }
    newSyll.appendChild(newDivTons)

    return newSyll
}