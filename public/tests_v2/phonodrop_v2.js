// https://jqueryui.com/draggable/#snap-to
// http://jsfiddle.net/fhamidi/PjmJe/1/

var group = [];
$('#multidraggable').multidraggable({
    containment: "#multidraggable",
    onCreate:(event, ui)=>{},

    onSelecting:(event, ui)=>{
        if (ui.selecting.id != 'tooldiv') console.log("Sélection ",ui.selecting.id)
    },
    onSelected:(event, ui)=>{
        if (ui.selected.id != 'tooldiv') group.push(ui.selected)
    },

    onStopSelecting:(event, ui)=>{
        if (group.length > 0) getTools(group)
    },
    onStartSelecting:(event, ui)=>{
        group = []
    },

    onUnselecting:(event, ui)=>{},
    onUnselected:(event, ui)=>{
        document.querySelector('#tooldiv').style.display = 'none'
    },

    onStartDrag:(event, ui, elements)=>{},
    onDrag:(event, ui, elements)=>{},
    onStopDrag:(event, ui, elements)=>{},
    snap: ".carte",
    stack: ".carte",
})

function sortGroup(group){
    return group.sort(function(a, b){ return a.getBoundingClientRect().x - b.getBoundingClientRect().x})
}

function addCarte(){
    var newCard = document.createElement('div')
    thisCardNb = document.querySelectorAll('.carte').length +1
    newCard.id = "carte" + thisCardNb
    newCard.classList = "carte"
    newCard.draggable = "true"
    newCard.innerHTML = thisCardNb
    document.getElementById('multidraggable').appendChild(newCard)
}

function initialize(){
    var listCards = document.querySelectorAll('.carte')
    listCards.forEach((card) => {
        card.classList.add('ui-selected', 'ui-draggable', 'ui-draggable-handle')
    })
    listCards.forEach((card) => {
        card.classList.remove('ui-selected', 'ui-draggable', 'ui-draggable-handle')
    })
}

function duplicate(groupX){
    group = groupX || group
    var sortedGroup = sortGroup(group)
    var mintop = 10000
    var maxtop = -10000
    sortedGroup.forEach(c => {
        mintop = ((c.getBoundingClientRect().top < mintop) ? c.getBoundingClientRect().top : mintop)
        maxtop = ((c.getBoundingClientRect().top > maxtop) ? c.getBoundingClientRect().top : maxtop)
    })
    var groupHeight = maxtop - mintop
    
    sortedGroup.forEach(card => {
        var newCard = card.cloneNode()
        newCard.id = card.id + '-' + Math.random().toString(36).substring(2,9);
        newCard.classList.remove('ui-selected')
        newCard.innerHTML = card.innerHTML
        newCard.style.left = card.getBoundingClientRect().left +'px'
        newCard.style.top = card.getBoundingClientRect().top + 80 + groupHeight +'px'
        // newCard.style.transform = "translateY("+groupHeight+"px)"
        
        document.getElementById('multidraggable').appendChild(newCard)
    })
}

function getGroupRect(groupX){
    group = groupX || group
    var mintop = 10000
    var maxbottom = -10000
    var minleft = 10000
    var maxright = -10000
    group.forEach(c => {
        mintop = ((c.getBoundingClientRect().top < mintop) ? c.getBoundingClientRect().top : mintop)
        maxbottom = ((c.getBoundingClientRect().bottom > maxbottom) ? c.getBoundingClientRect().bottom : maxbottom)
        minleft = ((c.getBoundingClientRect().left < minleft) ? c.getBoundingClientRect().left : minleft)
        maxright = ((c.getBoundingClientRect().right > maxright) ? c.getBoundingClientRect().right : maxright)
    })
    return {top:mintop, bottom:maxbottom, left:minleft, right:maxright, width:maxright-minleft, height:maxbottom-mintop}
}

function getTools(group){
    var groupRect = getGroupRect(group)
    
    var tooldiv = document.getElementById('tooldiv')
    tooldiv.style.top = groupRect.top -5+ 'px'
    tooldiv.style.height = groupRect.height +10+ 'px'
    tooldiv.style.left = groupRect.left -5+ 'px'
    tooldiv.style.width = groupRect.width +10+ 'px'

    document.querySelectorAll('.noMove').forEach(toto => { toto.style = "" })

    tooldiv.style.display = 'block'
}

function removeGroup(groupX){
    group = groupX || group
    group.forEach(card => { card.remove() })
    document.getElementById('tooldiv').style.display = 'none'
}

function alignGroup(groupX){
    group = groupX || group
    var groupRect = getGroupRect(group)
    group.forEach(card => {
        card.style.top = groupRect.top + groupRect.height/2 - card.getBoundingClientRect().height/2 + 'px'
    })
}

function reverseGroup(groupX){
    group = groupX || group
    var groupRect = getGroupRect(group)
    var ax = groupRect.right
    group.forEach(card => {
        cardleft = card.getBoundingClientRect().left
        card.style.left = ax +  ax - cardleft - groupRect.width - card.getBoundingClientRect().width + "px"
    })
}