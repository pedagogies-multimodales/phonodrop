const express = require('express');
const app = express();
const path = require('path');
const fs = require('fs');
const requestIp = require('request-ip');
const compression = require('compression');
const helmet = require('helmet');




app.use(express.json());
app.use(express.static(__dirname + '/public'));
app.use(compression()); // Compress all routes
app.use(helmet()); // Protection de base pour l'appli

// DÉCLARATION DES CHEMINS
const fileBaseDeDonnees = '../db/db_phonodrop.json';

// LECTURE DE LA BASE DE DONNÉES
var data = fs.readFileSync(fileBaseDeDonnees);
var baseDeDonnees = JSON.parse(data);
console.log('La base de données contient '+Object.keys(baseDeDonnees).length+' entrées.');


///////////////////////////////////////
//////// CHARGEMENT PAGE VIERGE ///////
///////////////////////////////////////
app.get('/', (req,res) => {
    res.sendFile(path.join(__dirname + '/public/index.html'));
});

///////////////////////////////////////
///// CHARGEMENT PAGE SAUVEGARDÉE /////
///////////////////////////////////////
app.get('/:id', (req,res) => {
    // VÉRIFICATION PRÉSENCE IDENTIFIANT RECHERCHÉ
    const contenu = baseDeDonnees[req.params.id];
    if (!contenu) {
        res.status(404).send("Cette page n'existe pas !");
    } else {
        res.sendFile(path.join(__dirname + '/public/index.html'));
    }
});

app.get('/:id/page', (req,res) => {
    // REVÉRIFICATION PRÉSENCE IDENTIFIANT RECHERCHÉ
    const contenu = baseDeDonnees[req.params.id];
    if (!contenu) {
        res.status(404).send("Error 404: cet identifiant ne renvoie à rien");
    } else {
        res.send(JSON.stringify(contenu));
    }
});

///////////////////////////////////////
///// SAUVEGARDE DE LA PAGE CLIENT ////
///////////////////////////////////////
app.post('/phonodrop/export', (request,response) => {
    var colis = request.body;
    colis.ipCli = requestIp.getClientIp(request);
    baseDeDonnees[colis.pageId] = colis;
    response.json({
        status: "success",
        identifiant:colis.pageId
    });
    fs.writeFile(fileBaseDeDonnees, JSON.stringify(baseDeDonnees, null, 2), finished);
    function finished(err) {
        console.log("Colis reçu depuis "+requestIp.getClientIp(request));
    }
    console.log('La base de données contient maintenant '+Object.keys(baseDeDonnees).length+' entrées.');
})

// PORT
const {port=8080} = process.env;
app.listen(port, () => console.log(`Listening on port ${port}...`));