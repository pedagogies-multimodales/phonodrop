# PhonoDrop

PhonoDrop est un outil permettant de manipuler des cartes phonologiques virtuelles. Chaque carte représente un phonème, et il est possible de les positionner librement, pour écrire des mots ou du texte sans recourir à la graphie. C’est un moyen de travailler l’oral par la manipulation, avant d’aborder les difficultés de l’écrit, et de découvrir les mécanismes phonologiques de la langue cible.

Plus d'informations ici : https://alem.hypotheses.org/outils-alem-app/phonodrop 

Langues actuellement disponibles : français, anglais britannique, chinois mandarin.

# Prérequis
Nécessite de démarrer un serveur NodeJS
```bash
sudo apt install nodejs
```

# Installation de PhonoDrop
```bash
git clone https://gricad-gitlab.univ-grenoble-alpes.fr/pedagogies-multimodales/phonodrop.git
cd phonodrop/
```

Initialisation d'un fichier de base de données PhonoDrop (chemin par défaut '../db_phonodrop.js')
```bash
mkdir ../db
echo "{}" > ../db/db_phonodrop.json
```

Démarrage du serveur
```bash
node index.js
```